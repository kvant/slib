#ifndef DATA_BUFFER_H
#define DATA_BUFFER_H

#include "pthread.h"
#include "stdint.h"
#include "semaphore.h"
#include "sys/types.h"

namespace slib
{

/* 
 * Class DataBuffer used if we need to share some data in asynchronous mode
 * betwen threads. We can use cicled or FIFO(blocked) policy.
 *
 */

  class DataBuffer
   {

  public:
         
      struct BufferItem 
         {
              void* data;
            size_t  size;
   struct timespec  push_time;
         };            

       enum BufferPolicy
         {
            BP_CICLED,
            BP_BLOCKED
         }; 

       enum BufferFlags
         {
           BF_NBLOCK   = 0x01<<0, 
           BF_TIME_ABS = 0x01<<1,
           BF_END_WAIT = 0x01<<2
         };  
  
      struct iovec
           {
            void* _data;
           size_t _size;           
           }; 

  protected:

 typedef int (DataBuffer::*push_method)  (void* data, size_t size, uint32_t flags);
 typedef int (DataBuffer::*push_methodv) (struct iovec* _vec, size_t _vec_size, uint32_t flags);
 typedef int (DataBuffer::*pull_method)  (void* data, size_t max_size, size_t& d_size, uint32_t flags); 

                    bool  Wait;
                  size_t  BuffSize;
                  size_t  ItemSize;
            BufferPolicy  BuffPolicy;
                   void*  PtrStorageBegin;
             BufferItem*  ItemsBegin;      
             BufferItem*  ItemsEnd;
             BufferItem*  ItemWrite;
             BufferItem*  ItemRead; 
             push_method  CurPushMethod;
             push_methodv CurPushMethodV;    
             pull_method  CurPullMethod;
     pthread_mutexattr_t  MutexAttr;
         pthread_mutex_t  SyncMutex;   
          pthread_cond_t  SelfCondVar;
                   sem_t  SyncSemaphore;

         void  IncPtr (BufferItem* &ptr) __attribute__((always_inline))  {
                 if (ptr!=ItemsEnd) ++ptr;  else ptr = ItemsBegin;       } 

          int  PushCicled    (void* data, size_t size, uint32_t flags);
          int  PushCicledV   (struct iovec* _vec, size_t _vec_size, uint32_t flags);  
          int  PullCicled    (void* data, size_t max_size, size_t& d_size, uint32_t flags);  

          int  PushBlocked   (void* data, size_t size, uint32_t flags);
          int  PushBlockedV  (struct iovec* _vec, size_t _vec_size, uint32_t flags);  
          int  PullBlocked   (void* data, size_t max_size, size_t& d_size, uint32_t flags);    

     public:
               DataBuffer    ();
               DataBuffer    (size_t n_itms, size_t itm_sz, BufferPolicy policy);    
              ~DataBuffer    ();
          int  FlushBuffer   ();
          int  InitBuffer    (void);
          int  SetItemSize   (size_t itm_sz);
          int  SetBufferSize (size_t n_itms);
          int  SetPolicy     (BufferPolicy policy);
       size_t  GetItemSize   (void) const;
       size_t  GetBufferSize (void) const;      
       size_t  GetNumItems   (void);
          int  GetPolicy     (void) const;
          int  IsEmpty       (void); 
          int  PushData      (void* data, size_t size, uint32_t flags);
          int  PushDataV     (iovec* vec, size_t _vec_size, uint32_t flags);     
          int  PullData      (void* data, size_t max_size, size_t& d_size, uint32_t flags);  
          int  PullDataTimed (void* data, size_t max_size, size_t& d_size, struct timespec f_time, uint32_t flags);    
   };
}

#endif
