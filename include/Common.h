#pragma once 

#include <string>
#include <netinet/in.h>

namespace slib    {
namespace common  {

// Common function used in config file parsing

int
StrOnlyNum(const char* str);

int
StrToUpper(char* str);

int
Str2SockAddr(std::string str, struct sockaddr_in& addr);

}//~common
}//~slib 
