
#define ALIGN(x) __attribute__ ((aligned (x)))
#define FIELD(type,name,align) type name ALIGN(align)
#define PACKED  __attribute__ ((packed))
#define BIT(x) 1<<x

#define FLOAT_RMS 1
#define FLOAT_VEL 1

#if (FLOAT_RMS)
 #define RMS_TYPE float
#else
 #define RMS_TYPE double
#endif

#if (FLOAT_VEL)
 #define VEL_TYPE float
#else
 #define VEL_TYPE double
#endif


/**
  *---------------------------------
  */

enum  CoordTypes
     {
      COORD_DECART,
      COORD_ANGLE,
      COORD_COSINUS,
      COORD_SPHERE,
      COORD_GEO    
     };  

/**
  *---------------------------------
  */

struct CoordDec3D
       {
        double X;
        double Y;
        double Z;   
       };  

/**
  *---------------------------------
  */
       
       
struct CoordGeo
      {
        double B;
        double L;
        double H;
      };     

/**
  *---------------------------------
  */
      
struct CoordSphere
      {
        double Azimuth;
        double Elevation;
        double Distance;   
      };  

/**
  *---------------------------------
  */
      
struct CrdVariant
      {
        uint8_t  CrdType;
        union{
        struct CoordGeo    Geo;
        struct CoordSphere Sph;
        struct CoordDec3D  Dec;
        } CrdData;
      };

/**
  *---------------------------------
  */
      
struct Velocity
      {
        VEL_TYPE Vabs;
        VEL_TYPE Vx;
        VEL_TYPE Vy;
        VEL_TYPE Vz;
      }; 

/**
  *---------------------------------
  */
      
struct RMS
      {
        RMS_TYPE RMSx;
        RMS_TYPE RMSy;
        RMS_TYPE RMSz;   
        RMS_TYPE RMSxy;        
        RMS_TYPE RMSxyz;
      };

/**
  *---------------------------------
  */
      
enum msgTypes
      {
        MSG_T_FRAME,            /*!< - message is frame (multimessage) */
        MSG_T_RAW_POINT,        /*!< - message is single point from source without track data */      
        MSG_T_TRACK_POINT,      /*!< - message is track point from source */
        MSG_T_TDS2SAMS,
        MSG_T_TRACK_POINT_INT
      };

/**
  *---------------------------------
  */
      
enum msgFeature 
      {
        MSG_F_NEXT_LOCATION = 0x0, /**< - message generated on next targen location */
        MSG_F_EXTRAPOLATED  = 0x1, /**< - message generated by extrapolation algorithm */
        MSG_F_TYPE_CHANGED  = 0x2, /**< - message generated by target type changed event*/
        MSG_F_NF_CHANGED    = 0x3, /**< - message generated by nationality feature changed event */
        MSG_F_TRACK_DROPED  = 0x4  /**< - message generated track is droped by source */    
      };

/**
  *---------------------------------
  */
      
enum loNationalityFeatures
      {
        LO_NF_UNKNOWN  = 0x00,
        LO_NF_ENEMY    = 0x01,
        LO_NF_FRIENDLY = 0x02  
      };

/**
  *---------------------------------
  */
      
enum msgFlags
      {
        MSG_HAS_REF_POINT  = BIT(0),
        MSG_HAS_MOD_NUMBER = BIT(1),
        MSG_HAS_SUB_TRACKS = BIT(2),
        MSG_TIME_INTERNAL  = BIT(3)
      };

/**
  *---------------------------------
  */
      
struct msgHeader
      {
        FIELD( uint8_t,             MsgHType,     1 ); // <--* enum msgTypes (alltimes MSG_T_FRAME)
        FIELD( uint8_t,             MsgSize,      1 ); // <--* full message size with header in 32bit words
        FIELD( uint8_t,             MsgSrc,       1 ); // <--* message source (0 - alltimes)
        FIELD( uint8_t,             MsgFlags,     1 ); // <--* enum msgFlags ( MSG_HAS_REF_POINT )
        FIELD( struct timespec,     LocTime,      2 ); // <--* location time (day,msec) if seted flags  MSG_TIME_INTERNAL - (tv_sec,tv_nsec)
      } PACKED;

/**
  *---------------------------------
  */
      
struct msgSubHeader
      {
        FIELD( uint8_t,             MsgType,      1 ); // <--* enum msgTypes
        FIELD( uint8_t,             ObjType,      1 ); // <--* not implemented now
        FIELD( uint8_t,             MsgFeatures,  1 ); // <--* enum msgFeature 
        FIELD( uint8_t,             Nationality,  1 ); // <--* enum loNationalityFeatures
        FIELD( uint8_t,             MsgFlags,     1 ); // ( MSG_HAS_MOD_NUMBER, MSG_HAS_SUB_TRACKS )
        FIELD( uint16_t,            ObjSrcNumber, 2 ); // <--* number of object in source ennumeration system
        FIELD( VEL_TYPE,            Vr,           2 ); // <--* radial velocity of location object  
      } PACKED;

/**
  *---------------------------------
  */
      
struct msgRawPoint
      {         
        FIELD( struct msgSubHeader, SubHeader,    4 ); // <--* 
        FIELD( float,               SN,           4 ); // <--* signal/noise (not used now)
        FIELD( RMS_TYPE,            RMS_az,       4 ); // <--* RMS of azimuth measurement
        FIELD( RMS_TYPE,            RMS_el,       4 ); // <--* RMS of elevetion angle measurement
        FIELD( RMS_TYPE,            RMS_dist,     4 ); // <--* RMS of distance measurement
        FIELD( struct CoordSphere,  CoordsSph,    4 ); // <--* sphere coordinates of location object
        FIELD( char,                SubData[],    4 );    
      } PACKED;

/**
  *---------------------------------
  */

typedef      
struct 
      {
        FIELD( uint16_t,            STInfo,         1 );
#define SRC_BITS  5        
#define MAKE_STINFO(src,track) (((src&0x1F)<<(16-SRC_BITS))|(track&0x7FF))
#define GET_SRC(info)   ((info>>(16-SRC_BITS))&0x1F)
#define GET_TRACK(info) (info&0x7FF)
      } SubTrack_t;

/**
  *---------------------------------
  */

struct fldSubTracks
      {
        FIELD( uint8_t,             NumST,        1 ); 
        FIELD( SubTrack_t,          SubTracks[],  1 );   
      } PACKED;

/**
  *---------------------------------
  */

struct msgTrackPoint
      { 
        FIELD( struct msgSubHeader, SubHeader,    4 ); // <--*
        FIELD( uint32_t,            Height,       4 ); // <--* height in meters
        FIELD( struct CrdVariant,   Coords,       4 ); // <--* ccordinates of location object
        FIELD( struct Velocity,     V,            4 ); // <--* velocity of location object
        FIELD( struct RMS,          RMSCrd,       4 ); // <--* RMS of coordinates measurement
        FIELD( struct RMS,          RMSVel,       4 ); // <--* RMS of velocity measurement
        FIELD( char,                SubData[],    4 );   
      } PACKED;

/**
  *---------------------------------
  */
      
struct msgGeneral
      {
        FIELD( struct msgHeader,    Header,       4);  // <--* message header
        FIELD( char,                Data[],       4);  // <--* message data
      } PACKED;
      
/**
  *---------------------------------
  */
    
/**
  *  
  */

enum samsFireMode
 {
  SM_MISSILE = 1,
  SM_DBLMISSILE,
  SM_VOLLEY,
  SM_VOLLEY_MISSILE,
  SM_ART,
  SM_ART_MISSILE,
  SM_ART_VOLLEY,
 };
 
 enum samsWorkMode
 {
  SM_BLOCK  = BIT(0),
  SM_ASSIGN = BIT(1),
  SM_DROP   = BIT(2)
 };

struct msgTARGETData
    {
            FIELD ( struct CoordSphere, m_TargSphCrd, 1 );    // target replacement at LocTime
            FIELD ( struct Velocity,    m_V,          1 );    // target velocity components at LocTime
            FIELD ( VEL_TYPE,           m_Vr,         1 );    // radial velocity of target
    } PACKED; 
 
struct msgTDS2SAMS
    {
            FIELD ( uint8_t, m_MsgType,               1 );   // MSG_T_TDS2SAMS 
            FIELD ( uint8_t, m_SamsId,                1 );   // id SAM system
            FIELD ( uint8_t, m_ChanId,                1 );   // id SAM channell
            FIELD ( uint8_t, m_FF,                    1 );   // features&flags (flags only MSG_HAS_MOD_NUMBER)
#define SET_FLAGS(msg,flags)  if(1){ msg.m_FF&=0x0F; msg.m_FF|=((flags&0x0F)<<4); }
#define GET_FLAGS(msg)  ((msg.m_FF>>4))
            FIELD ( uint8_t, m_FireMode,              1 );   // enum samsFireMode
            FIELD ( uint8_t, m_WorkMode,              1 );   // enum samsWorkMode 
            FIELD ( struct msgTARGETData, m_Target[], 1 );   //target info (if in m_WorkMode present SM_ASSIGN)
    } PACKED; //in normal size if 8 bytes , with m_Target summ size is 52 bytes

/*
 
//  - � ������ ������� ��������� ���� ��������� struct msgHeader,
//  - ���������  ����� ��������� ���� ��� ��������� ��������� ����� ��� ��������� ������������
//  - ��������� � ������ ������ ����������� ���� �� ������ � ���������� � ���� uint8_t m_MsgType
//  - ���� ���������� ���� MSG_HAS_REF_POINT � struct msgHeader.MsgFlags, �� ����� �� ���������� struct msgHeader ������� ��������� ������ struct CrdVariant � �������
//    ������������� ���������� ��������� ��������� ������� �� ������ ������������ ���������
//  - ���� ���������� ���� MSG_HAS_MOD_NUMBER � struct msgSubHeader.MsgFlags, ( ��� m_FF ��� msgTDS2SAMS )�� ����� �� ���������� ������� ��������� �����
//    ���� (uint16_t)
//  - ���� ���������� ���� MSG_HAS_SUB_TRACKS � struct msgSubHeader.MsgFlags, �� ����� �� struct msgTrackPoint ������� ������ ������� ����� �� ������� �������
//    ������������� ������ ������. ������: struct fldSubTracks.

*/
