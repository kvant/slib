#ifndef SLIB_CORE_H
#define SLIB_CORE_H

#include <DataBuffer.h>
#include <UDPPoller.h>
#include <TimeSupport.h>
#include <Common.h>
#include <Geo.h>
#include <Algorithms.h>
#include <Proto.h>
#include <Log.h>
#include <Config.h>
#include <TimeSystem.h>
#include <Navigation.h>
#include <DBuffer.h>
#endif
