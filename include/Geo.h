#ifndef __GEO_H
#define __GEO_H

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#ifndef FLOAT_RMS
#define FLOAT_RMS	0
#endif

#ifndef FLOAT_VEL
#define FLOAT_VEL	0
#endif

#ifndef FLOAT_ANGLES
#define FLOAT_ANGLES	0
#endif

#if (FLOAT_RMS)
 #define RMS_TYPE float
#else
 #define RMS_TYPE double
#endif

#if (FLOAT_VEL)
 #define VEL_TYPE float
#else
 #define VEL_TYPE double
#endif

#if (FLOAT_ANGLES)
 #define ANGLE_TYPE float
#else
 #define ANGLE_TYPE double
#endif

/*
 * Some macros for make easy some calculation
 */

#define GEO_TO_RAD(D,M,S) ((D*3600+M*60+S)*M_PI/648000.0)
#define RAD_TO_DEG(R) (180*R/M_PI)
#define DEG_TO_RAD(D) (M_PI*D/180.0)
#define GET_DEG(R) (int)floor(RAD_TO_DEG(R))
#define GET_MIN(R) (int)floor((RAD_TO_DEG(R)-GET_DEG(R))*60)
#define GET_SEC(R) (int)floor((RAD_TO_DEG(R) - (GET_DEG(R)+GET_MIN(R)/60.0))*3600)
#define NORMALIZE_ANGLE(angle) { angle = (angle >= 0 && angle < M2PI) ? angle : \
					 (angle < 0 ? ( M2PI + fmod(angle, M2PI)) : \
					  fmod(angle,M2PI)); \
				}
#define GET_NORMALIZE_ANGLE(angle) (angle>=0 && angle<M2PI)?angle:(angle<0?(M2PI+fmod(angle,M2PI)):(fmod(angle,M2PI)-M2PI))
#define IN_DIA(min,max,val) ((val >= min) && (val <= max))

#define  M2PI       6.28318530718
#define  M_3PI_2    4.71238898038
#define  SQRT_2PI   2.506628274631
#define  SQRT_2PI_1 0.3989422804014


namespace slib {
namespace geo  {

/*! \brief Brief description.
 *         Brief description continued.
 *
 *  Detailed description starts here.
 */

enum  CoordTypes {
	COORD_DECART,
	COORD_ANGLE,
	COORD_COSINUS,
	COORD_SPHERE,
	COORD_GEO,
};

typedef double CMatrix_t[4][4];

struct CoordDec2D {
	double X;
	double Y;
};

struct CoordDec3D {
	double X;
	double Y;
	double Z;
};

struct CoordGeo {
	double B;
	double L;
	double H;
};

struct CoordSphere {
	double Azimuth;
	double Elevation;
	double Distance;
};

struct CrdVariant {
	uint8_t  CrdType;
	union {
		struct CoordGeo    Geo;
		struct CoordSphere Sph;
		struct CoordDec3D  Dec;
	} CrdData;
};

struct VecDec3d {
	double X[2];
	double Y[2];
	double Z[2];
};

struct Velocity {
	VEL_TYPE Vabs;
	VEL_TYPE Vx;
	VEL_TYPE Vy;
	VEL_TYPE Vz;
};

struct RMSsph {
	ANGLE_TYPE RMSa;
	ANGLE_TYPE RMSe;
	RMS_TYPE RMSd;
};

struct RMS {
	RMS_TYPE RMSx;
	RMS_TYPE RMSy;
	RMS_TYPE RMSz;
	RMS_TYPE RMSxy;
	RMS_TYPE RMSxyz;
};

struct EllipsoidParams {
	double a;
	double a2;
	double b;
	double b2;
	double e12;
	double B_Precise;
};

struct LineXYZ {     /* Simple class for 3d line interpretation*/
	double			m_K;
	double			m_Ko;
	double			m_B;
	struct CoordDec3D	m_Pnt[2];
	LineXYZ (struct CoordDec3D a, struct CoordDec3D b );
	LineXYZ (){};
	double inline X(double y) {return (y - m_B) * m_Ko;};
	double inline Y(double x) {return (x * m_K + m_B);};
	bool inline iDx(double x) {return ((x >= m_Pnt[0].X && x <= m_Pnt[1].X) || (x <= m_Pnt[0].X && x >= m_Pnt[1].X));};
	bool inline iDy(double y) {return ((y >= m_Pnt[0].Y && y <= m_Pnt[1].Y) || (y <= m_Pnt[0].Y && y >= m_Pnt[1].Y));};
};

struct GeoRect {
	enum LinesInd { L_01, L_12, L_23, L_30,L_NA };

	bool			m_XYZValid;
	struct CoordGeo		m_PointsBLH[4];
	struct CoordGeo		m_Center;
	struct CoordDec3D	m_PointsXYZ[4];
	struct LineXYZ		m_Lines[4];
	struct CoordDec3D	m_LeftBottom;
	struct CoordDec3D	m_RightTop;

	GeoRect() { };
	GeoRect(struct CoordGeo dots[4]);
	void PrepareXYZ(struct CoordGeo& center);
	void InitByXYZ(struct CoordDec3D dots[4]);
	bool IsPointInRect(struct CoordDec3D& point);
	bool IsPointInRect(struct CoordGeo& point, struct CoordGeo& center);
	bool IsPointInRect(struct CoordGeo& point);
};


//=============================================================================

void InitEllipsoide(int el_type);
void ConvBLH2XYZ(struct CoordGeo& geo, struct CoordDec3D& dec);
void ConvXYZ2BLH(struct CoordDec3D& dec, struct CoordGeo& geo);
void ConvXYZ2XYZ(struct CoordDec3D& center,struct CoordDec3D& dest, CMatrix_t& matrix);
void ApplyMatrix(struct CoordDec3D& xyz_meters, CMatrix_t& matrix);
void ApplyMatrixXYZ(struct CoordDec3D& xyz_meters, CMatrix_t& matrix);
void ConvXYH2XYZ(struct CoordDec3D& xyh, struct CoordGeo& src);
void CalcCMatrix(struct CoordGeo& src, struct CoordGeo& dest, CMatrix_t& matrix);
void CalcCMatrix(struct CoordGeo& src, CMatrix_t& matrix);
void CalcCMatrix(struct CoordGeo& src, struct CoordDec3D& dec, CMatrix_t& matrix);
void xyz2BLH(struct CoordGeo& src_pos, struct CoordDec3D& obj_pos_xyz, struct CoordGeo& obj_pos_blh);
void BLH2xyz(struct CoordGeo& center, struct CoordGeo& obj_pos_blh, struct CoordDec3D& obj_pos_xyz);
void TranslateV_kms(struct Velocity& V_src, struct Velocity& V_dst, CMatrix_t& matrix);
void TranslateRMS_km(struct RMS& RMS_src, struct RMS& RMS_dst, CMatrix_t& matrix);

struct CoordDec3D inline operator+(const struct CoordDec3D& a, const struct CoordDec3D& b)
{
	struct CoordDec3D c = {
		a.X + b.X,
		a.Y + b.Y,
		a.Z + b.Z
	};
	return c;
}

struct CoordDec3D inline operator-(const struct CoordDec3D& a, const struct CoordDec3D& b)
{
	struct CoordDec3D c = {
		a.X - b.X,
		a.Y - b.Y,
		a.Z - b.Z
	};
	return c;
}


struct CoordDec3D inline &operator+=(struct CoordDec3D& a, const struct CoordDec3D& b)
{
	a.X += b.X;
	a.Y += b.Y;
	a.Z += b.Z;
	return a;
}

ANGLE_TYPE inline GetAzimuth(struct CoordDec3D& crd)
{
	return ((crd.X == 0.0) ? (crd.Y >= 0.0 ? M_PI_2 : M_3PI_2) :
			((crd.X > 0.0) ? ((crd.Y >= 0.0) ? atan(crd.Y / crd.X) :
				(M2PI + atan(crd.Y / crd.X))) :
			 		(M_PI + atan(crd.Y / crd.X))));
}

ANGLE_TYPE inline GetCourse(double Xa, double Ya, double Xb, double Yb)
{
 	Xb -= Xa;
 	Yb -= Ya;
 	return ((Yb == 0.0) ? (Xb >= 0.0 ? M_PI_2 : M_3PI_2) :
			((Yb > 0.0) ? ((Xb >= 0.0) ? atan(Xb / Yb) :
				(M2PI + atan(Xb / Yb))) : (M_PI + atan(Xb / Yb))));
}

ANGLE_TYPE inline GetCoursePVO(double Xa, double Ya, double Xb, double Yb)
{
 	Xb -= Xa;
 	Yb -= Ya;
 	return ((Xb == 0.0) ? (Yb >= 0.0 ? M_PI_2 : M_3PI_2) :
			((Xb > 0.0) ? ((Yb >= 0.0) ? atan(Yb / Xb) :
				(M2PI + atan(Yb / Xb))) : (M_PI + atan(Yb / Xb))));
}

double inline GetDistance(double Xa, double Ya, double Xb, double Yb)
{
	Xb -= Xa;
	Yb -= Ya;
	return (sqrt(Xb * Xb + Yb * Yb));
}

double inline GetDistance(struct CoordDec3D& p1)
{
	return (hypot(hypot(p1.X, p1.Y), p1.Z));
}

double inline GetDistance(struct CoordDec3D& p1, struct CoordDec3D& p2)
{
	return (hypot(hypot(p1.X - p2.X, p1.Y - p2.Y), p1.Z - p2.Z));
}

void inline RotatePoint (struct CoordDec3D& pnt, ANGLE_TYPE a)
{
	double x = pnt.X * cos(a) + pnt.Y * sin(a);
	double y = pnt.Y * cos(a) - pnt.X * sin(a);
	pnt.X = x;
	pnt.Y = y;
}

double inline GetCourseByV(struct Velocity& _v)
{
	if (_v.Vy == 0.0)
		return (_v.Vx >= 0.0) ? M_PI_2 : M_3PI_2;
	return(_v.Vy > 0.0) ? ((_v.Vx >= 0.0) ? atan(_v.Vx / _v.Vy) :
			(M2PI + atan(_v.Vx / _v.Vy))) : (M_PI + atan(_v.Vx / _v.Vy));
}

double inline GetCourseParameter(struct CoordDec3D& lo_pos, ANGLE_TYPE course)
{
	return  fabs(hypot(lo_pos.X, lo_pos.Y) * sin(course - (M_PI + (GetCourse(0, 0, lo_pos.X, lo_pos.Y)))));
}

bool inline IsApproaching(struct CoordDec3D& lo_pos, ANGLE_TYPE course)
{
	double c = fabs(GetCourse(lo_pos.X, lo_pos.Y, 0, 0) - course);
	NORMALIZE_ANGLE(c);
	return (((c > M_PI) ? (M2PI - c) : c) < M_PI_2);
}

double inline GetVRadial(struct CoordDec3D &pos, struct Velocity &v)
{
	return (pos.X * v.Vx + pos.Y * v.Vy + pos.Z * v.Vz) /
		sqrt(pos.X * pos.X + pos.Y * pos.Y + pos.Z * pos.Z);
}

double inline GetATime(struct CoordDec3D &pos, struct Velocity &v)
{
	struct CoordDec3D np = {0};
	return -(GetDistance(np, pos) / GetVRadial(pos, v));
}


double inline DeltaAngleDeg(double a, double b)
{
	a = fabs(a - b);
	return (( a > M_PI) ? (M2PI - a) : a);
}

double inline DeltaAngleRad(double a, double b)
{
	a = fabs(a - b);
	return (a > M_PI) ? fabs(a - M2PI)  : a;
}

bool inline IsInSector(ANGLE_TYPE sec_b,ANGLE_TYPE sec_e,ANGLE_TYPE val)
{
	return ((sec_b > sec_e) ? (!(val > sec_e && val < sec_b)) : (val >= sec_b && val <= sec_e));
}

void CrossCircle(double Px, double Py, double q,double R, std::vector<CoordDec2D>& pnt_vec);
int CrossLines(CoordDec2D p11, ANGLE_TYPE q1, CoordDec2D p21, ANGLE_TYPE q2, CoordDec2D& cr_p);
int CrossLinesSharp(CoordDec2D p11, CoordDec2D p12, CoordDec2D p21, CoordDec2D p22, CoordDec2D& cr_p);

void inline SphRMS2DecRMS(double R_az, double R_el, double R_d, struct CoordSphere& _crd, struct RMS& _rms)
{
	double a_SigmaD2       = R_d*R_d;
	double a_SigmaBetta2   = R_az*R_az;
	double a_SigmaEpsilon2 = R_el*R_el;
	double a_D2            = _crd.Distance*_crd.Distance;
	double a_CosBetta2     = pow (cos (_crd.Azimuth),2);
	double a_SinBetta2     = pow (sin (_crd.Azimuth),2);
	double a_CosEpsilon2   = pow (cos (_crd.Elevation),2);
	double a_SinEpsilon2   = pow (sin (_crd.Elevation),2);

	_rms.RMSx = sqrt (a_SigmaD2 * a_CosBetta2 * a_CosEpsilon2 +
	a_D2 * a_SinBetta2 * a_CosEpsilon2 * a_SigmaBetta2 +
	a_D2 * a_CosBetta2 * a_SinEpsilon2 * a_SigmaEpsilon2);

	_rms.RMSy = sqrt (a_SigmaD2 * a_SinBetta2 * a_CosEpsilon2 +
	a_D2 * a_CosBetta2 * a_CosEpsilon2 * a_SigmaBetta2 +
	a_D2 * a_SinBetta2 * a_SinEpsilon2 * a_SigmaEpsilon2);

	_rms.RMSz = sqrt (a_SigmaD2 * a_SinEpsilon2 +
	a_D2 * a_CosEpsilon2 * a_SigmaBetta2);

#if 0
	_rms.RMSx*=1000;
	_rms.RMSy*=1000;
	_rms.RMSz*=1000;
#endif
}

static inline void Dec3D2Sphere(struct CoordDec3D& dec, struct CoordSphere& sph)
{
	sph.Distance  = hypot(dec.X, hypot(dec.Y, dec.Z));
	sph.Elevation = dec.Z / sph.Distance;
	sph.Azimuth   = GetCourse(0, 0, dec.X, dec.Y);
}

static inline void Sphere2Dec3D(struct CoordSphere& sph, struct CoordDec3D& dec)
{
	double Es,Ec;
	double As,Ac;
	sincos(sph.Elevation, &Es, &Ec);
	sincos(sph.Azimuth, &As, &Ac);

	dec.X = sph.Distance * Ec * As;
	dec.Y = sph.Distance * Ec * Ac;
	dec.Z = sph.Distance * Es;
}

static inline double CalcV3D(CoordDec3D &a, CoordDec3D &b, double dtime, Velocity& V)
{
	V.Vx = (b.X - a.X) / dtime;
	V.Vy = (b.Y - a.Y) / dtime;
	V.Vz = (b.Z - a.Z) / dtime;
	return V.Vabs = sqrt(V.Vx * V.Vx + V.Vy * V.Vy + V.Vz * V.Vz);
}

}
}

#endif
