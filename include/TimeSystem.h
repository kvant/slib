#pragma once
#include "pthread.h"
#include "stdint.h"
#include <set>
#include <TimeSupport.h>
#include <netinet/in.h>

extern "C"
{

 struct uts_time_val
  {
    uint32_t  day;
    uint32_t  msec;
  };

int uts_open     ( void );
int uts_close    ( void );
int uts_get_time ( struct uts_time_val* );

};

namespace slib     {
namespace services {

class TimeSystem
{
 public:

 enum TSMode {
	TSM_NET_LISTENER,
	TSM_PCI_ACCESS,
	TSM_NOT_VALID
};

enum TEFlags {
	TE_FL_TIME_RELATIVE = 0x01
};

struct timeEvent {
	struct timespec	te_time;
	unsigned  	te_type;
	void		*te_arg1;
	void		*te_arg2;
	void		(*te_cb)(struct timeEvent *te);
};

struct NetModeOpts {
	uint16_t ListenPort;
};

struct PCIModeOpts {
	struct timespec UpdatePeriod;
};


struct TSInitData_t {
	TSMode	m_Mode;
	bool	m_EventsEnable;
	union {
		struct NetModeOpts Net;
		struct PCIModeOpts PCI;
	}	m_Opts;
};

 protected:

                     TimeSystem ();
                     TimeSystem (const TimeSystem& src);
        TimeSystem&  operator=  (const TimeSystem& src);

  public:

 static TimeSystem*  NewTimeSystem   ( struct TSInitData_t& _init_data, bool autostart = true );

   virtual          ~TimeSystem();
   virtual      int  Start           (struct TSInitData_t* _init_data = NULL)      = 0;
   virtual      int  Stop            (void)                                        = 0;
   virtual uint32_t  InsertEvent     (struct timeEvent& event, uint flags = 0)     = 0;
   virtual     void  FlushEvents     (void)                                        = 0;
   virtual     void  RemoveEvent     (uint32_t id, bool locked = true)             = 0;
   virtual     void  GetCurrentTime  (struct timespec& _time)                      = 0;
   virtual
    struct timespec  GetCurrentTime  ( void )                                        = 0;
   virtual     void  GetCurrentTime  ( struct slib::timesupport::timepascal& _time ) = 0;
   virtual     void  Wait4Ready      ( void )                                        = 0;

   virtual     void  InsertRepeatEP  ( struct sockaddr_in& addr )                    = 0;
   virtual     void  RemoveRepeatEP  ( struct sockaddr_in& addr )                    = 0;
   virtual     void  DropAllRepeatEP ( void )                                        = 0;
};

}
}

