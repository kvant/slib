#pragma once

#include <vector>
#include <map>
#include <set>
#include <stdint.h>

#define INFINITE_RANGE 1e8

namespace       slib {
namespace algorithms {

/*! Matrix simple C++ structure */
struct DMatrix
             {
			DMatrix(); /*! Empty matrix constructor*/
			DMatrix(uint32_t _r, uint32_t _c);
		       ~DMatrix();
	         void   Init(uint32_t _r, uint32_t _c);
	         void   Init(uint32_t _r, uint32_t _c, double _def_val); //init matrix and fill it with _def_val
		 void   SaveRC(void);
		 void   RestoreRC(void);
	     uint32_t   Rows;
	     uint32_t   Columns;
std::vector<uint32_t>   ERows;
std::vector<uint32_t>   EColumns;
std::vector<uint32_t>   sERows;
std::vector<uint32_t>   sEColumns;
             double**   data;
		char*   dptr;
	     };

 struct MatrixDecision
             {
                                    double   Result;
 std::vector<std::pair<uint32_t, uint32_t> >  Conformance;
                        std::set<uint32_t>   UnresolvedRows;
                        std::set<uint32_t>   UnresolvedColumns;
             };



//!dynamic programming section
/*!
  Computing matrux with dynamicaly programming method
  by Rows 
  \param matrix - matrix structure \see DMatrix
*/
void dpComputeByRows (struct DMatrix& matrix, struct MatrixDecision& decision);
void dpComputeByColumns (struct DMatrix& matrix, struct MatrixDecision& decision);

//Do debug matrix dump
void dpDumpMatrix(struct DMatrix& _m);

}
}
