#ifndef SLIB_TIME_H
#define SLIB_TIME_H

#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define TIME_NSEC 1000000000
#define TSPEC2DBL(a) (a.tv_sec+a.tv_nsec*1e-9)
#define DBL2TSPEC(a,b)  {b.tv_sec = floor(a); b.tv_nsec=1e9*(a-floor(a));}
#define USEC2TSPEC(a,b) {b.tv_sec = (uint32_t)floor(double(a)/1000.0); b.tv_nsec = (a%1000)*1000;}
#define DBL2TSPEC_INIT(a) {(time_t)a, (time_t)(1e9 * (a - (time_t)a))}

#define TPAS_ACCURACY 1000000

namespace slib {
namespace timesupport {

struct timepascal
   {
     uint32_t tp_usec; 
     uint32_t tp_day;  
   };


//================================================================================

void 
__inline__
tpascal2tspec(struct timepascal& tp,struct timespec& ts)
{
 register uint a = uint(floor(tp.tp_usec * 0.001));
 ts.tv_sec  = 86400 * tp.tp_day + a;
 ts.tv_nsec = (tp.tp_usec - a*1000)*1000000;
}

//================================================================================

void __inline__ tspec2tpascal(struct timespec &ts,struct timepascal &tp)
{
	tp.tp_day  = uint32_t(floor(ts.tv_sec / 86400.0));
	tp.tp_usec = (ts.tv_sec - (tp.tp_day * 86400)) * 1000 + ts.tv_nsec * 0.000001;
}

//=================================================================================

struct timespec
__inline__
operator+(const struct timespec& a,const struct timespec& b)
{
  struct timespec c = a;
  c.tv_sec+=b.tv_sec;
  c.tv_nsec+=b.tv_nsec;
  if(c.tv_nsec>=TIME_NSEC)
      {
       ++c.tv_sec;   
         c.tv_nsec-=TIME_NSEC;
      }
  return c;      
}

//=================================================================================

bool __inline__ operator == (const struct timespec &a, const struct timespec &b)
{
	return (a.tv_nsec == b.tv_nsec && a.tv_sec == b.tv_sec);
}

//=================================================================================

int __inline__ operator > (const struct timespec &a, const struct timespec &b)
{
 if (a.tv_sec ^ b.tv_sec) return (a.tv_sec > b.tv_sec);
                          return (a.tv_nsec > b.tv_nsec);
}

//=================================================================================

int __inline__ operator < (const struct timespec &a, const struct timespec &b)
{
 if (a.tv_sec ^ b.tv_sec) return (a.tv_sec < b.tv_sec);
                        return (a.tv_nsec < b.tv_nsec);
}

//=================================================================================

bool
__inline__
operator<=(const struct timespec& a, const struct timespec& b)
{
 return (a.tv_sec^b.tv_sec)?(a.tv_sec<=b.tv_sec):(a.tv_nsec<=b.tv_nsec); 
}

//=================================================================================

bool
__inline__
operator>=(const struct timespec& a, const struct timespec& b)
{
 return (a.tv_sec^b.tv_sec)?(a.tv_sec>=b.tv_sec):(a.tv_nsec>=b.tv_nsec);  
}

//=================================================================================

struct timespec
__inline__
operator*(struct timespec& a, double c)
{
 struct timespec res;
 double val = TSPEC2DBL(a);
 val*=c;
 DBL2TSPEC(val,res);
 return res;  
}

//=================================================================================

inline struct timespec& operator+=(struct timespec& a, const struct timespec& b)
{
 a.tv_sec+=b.tv_sec;
 a.tv_nsec+=b.tv_nsec;
 if(a.tv_nsec>=TIME_NSEC)
   {
    ++a.tv_sec;
      a.tv_nsec-=TIME_NSEC;
   }    
 return a;
}

//=================================================================================

inline struct timespec& operator+=(struct timespec& a, const double &b)
{
	struct timespec tsb = DBL2TSPEC_INIT(b);
	return (a+=tsb);
}


//=================================================================================

struct timespec
__inline__
operator-(const struct timespec& a,const struct timespec& b)
{
// printf("%s\n",__FUNCTION__);
 struct timespec c = a;
 c.tv_sec-=b.tv_sec;
 c.tv_nsec-=b.tv_nsec;
 if (c.tv_nsec<0)
   {
      --c.tv_sec;
        c.tv_nsec+=TIME_NSEC; 
   }
 return c;      
}

//===============================================================================

inline
struct timespec&
operator*=(struct timespec& a, double b)
{
 double val = TSPEC2DBL(a);
 val*=b;
 DBL2TSPEC(val,a);  
 return a;
}

//==================================================================================
__inline__
struct timespec&
operator-=(struct timespec& a, const struct timespec& b)
{
 a.tv_sec -= b.tv_sec;
 a.tv_nsec-= b.tv_nsec;
 if (a.tv_nsec<0)
   {
      --a.tv_sec;
        a.tv_nsec+=TIME_NSEC;  
   }
 return a;      
}

//==================================================================================

double 
__inline__
operator/(const struct timespec &a, const struct timespec &b)
{
 double _a = TSPEC2DBL(a);
 double _b = TSPEC2DBL(b);
 return _a/_b;
}

//==================================================================================

inline 
void
time_wait(struct timespec& abs_time)
{
 struct timespec cur_time;
 clock_gettime(CLOCK_REALTIME,&cur_time);
 if (abs_time<cur_time) return;
 cur_time = abs_time-cur_time;
 nanosleep(&cur_time,NULL);
}

//==================================================================================

bool 
__inline__
operator !=(const struct timespec& a, const struct timespec& b)
{
 return (a.tv_sec^b.tv_sec || a.tv_nsec^b.tv_nsec)?true:false;
}

}}

#endif
