#pragma once

#include <Proto.h>
#include <string>
#include <iostream>

#define LOGI(log, ...) log << info << __VA_ARGS__ << endr
#define LOGE(log, ...) log << error << __VA_ARGS__ << endr
#define LOGW(log, ...) log << warn  << __VA_ARGS__ << endr
#define LOGD(log, ...) log << debug << __VA_ARGS__ << endr

namespace slib {
namespace log  {

using namespace std;

void logTrPoint2Str(struct proto::msgTrackPoint _arg, std::string& _out_str);

//================================================================================================

class SLog_p;
class SLogSess_p;

enum LogOperators
      {
         LOG_OP_BEGIN_ERR,
         LOG_OP_BEGIN_INFO,
         LOG_OP_BEGIN_WARN,
         LOG_OP_BEGIN_DBG,
         LOG_OP_FINISH
      };

class SLog
{
    SLog_p* d_ptr;

   public:

   enum Flags
         {
           LF_TIMED       = 1<<0,
           LF_NAMED       = 1<<1,    
           LF_LEFT_SPACE  = 1<<2,
           LF_RIGHT_SPACE = 1<<3,      
           LF_ENABLE_ERR  = 1<<4,
           LF_ENABLE_INFO = 1<<5,
           LF_ENABLE_WARN = 1<<6, 
           LF_ENABLE_DBG  = 1<<7   
         };

  class SLogSess
   {
    friend class SLog;  
    friend class SLog_p;
    friend class SLogSess_p;
    friend       SLogSess& LOperator(SLogSess&,LogOperators, uint32_t level); 
 
     SLogSess_p* d_ptr;    
         
     public:
       SLogSess ();
       SLogSess (const SLogSess& src);
      ~SLogSess ();
       SLogSess& operator<<( int a );
       SLogSess& operator<<( double a );
       SLogSess& operator<<( const char* );
       SLogSess& operator<<( const string& str );  
       SLogSess& operator<<( SLogSess&(*a)(SLogSess& src));     
            void SetName (const char* name);
  };

  public: 

                SLog();
               ~SLog();
   
inline static
         SLog&  Instance      ( void ) { static SLog a; return a; }
                           
          void  SetLogLevel   ( int level );
           int  GetLogLevel   ( void ) const;
          void  SetLogFlags   ( uint32_t flags );
      uint32_t  GetLogFlags   ( void ) const;  
     
     SLogSess&  CreateSession ( const char* name, int def_level, const char* f_name );
           int  AliasSession  ( SLogSess& sess, const char* f_name );
          void  CloseSession  ( SLogSess& sess );
};

SLog::SLogSess& LOperator(SLog::SLogSess& sess, LogOperators oper, uint32_t level = 0);

__inline__ SLog::SLogSess& info   ( SLog::SLogSess& sess )  { return LOperator(sess,LOG_OP_BEGIN_INFO); }
__inline__ SLog::SLogSess& warn   ( SLog::SLogSess& sess )  { return LOperator(sess,LOG_OP_BEGIN_WARN); }
__inline__ SLog::SLogSess& debug  ( SLog::SLogSess& sess )  { return LOperator(sess,LOG_OP_BEGIN_DBG);  }
__inline__ SLog::SLogSess& error  ( SLog::SLogSess& sess )  { return LOperator(sess,LOG_OP_BEGIN_ERR);  }
__inline__ SLog::SLogSess& endr   ( SLog::SLogSess& sess )  { return LOperator(sess,LOG_OP_FINISH);     }

}
}

