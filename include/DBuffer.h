#ifndef _DBUFFER_H
#define _DBUFFER_H

#include <semaphore.h>
#include <pthread.h>
#include <stdint.h>

class DBuffer
{
  struct Item
      {
              void* data;
            size_t  size;
      struct Item*  Next;
      };

	struct Item* Data;
	struct Item* Rd;
	struct Item* Wr;

	sem_t SemRev;
	sem_t SemFwd;
	size_t BSize;
	size_t ISize;
pthread_mutex_t Mutex;

  public:
	void inline Lock()   { pthread_mutex_lock(&Mutex); }
	void inline UnLock() { pthread_mutex_unlock(&Mutex); }
        DBuffer(size_t _items, size_t _itm_size);
       ~DBuffer();
   int  Push  (void* data, size_t size, bool no_block = false);
   int  Pop   (void* data, size_t max_size, size_t& r_size, bool no_block = false);
  void  Reset (void);
};

#endif
