/**
   Config.h
   Classes for create,read, modification and store configuration files 
   (C) Ryazantsev Vladimir
       2009
       v.ryazantsev@rambler.ru
*/
#pragma once

#include <cstdlib>
#include <cstdio>
#include <stdint.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <netinet/in.h>

namespace slib {
namespace config  {
using namespace std;


class Config;
class ConfigSection;

class ConfigSection
   {

     friend class Config;
     protected:
                  ConfigSection();

     public:

                virtual      ~ConfigSection();
  static    ConfigSection*   CreateSection     (const char* name);
  static             void    DestroySection    (ConfigSection* section);

 virtual              int    ParseFromStream   (ifstream& data)                                       = 0;
 virtual              int    SaveToStream      (ofstream& data)                                       = 0;

 virtual         uint32_t    GetNumValues      (const char* name)                                     = 0;
 virtual         uint32_t    GetNumSections    (const char* name)                                     = 0;
 virtual             void    GetValuesNames    (vector<string>& vstr)                                 = 0;
 virtual             void    GetSectionsNames  (vector<string>& vstr)                                 = 0;

 virtual    ConfigSection*   GetSection        (const char* name , uint32_t id)                       = 0;
 virtual    ConfigSection*   GetSectionEx      (const char* name)                                     = 0;
 virtual             void    RmSection         (const char* name, uint32_t id)                        = 0;
 virtual             void    RmSectionArr      (const char* name)                                     = 0;
 virtual    ConfigSection*   AddSection        (const char* name)                                     = 0;
 virtual              int    AddValue          (const char* name)                                     = 0;
 virtual              int    RmValue           (const char* name, uint32_t id)                        = 0;
 virtual              int    RmValueArr        (const char* name)                                     = 0;
 virtual              int    SetInt            (const char* name, uint32_t id, int val)               = 0;
 virtual              int    GetIntV           (const char* name, uint32_t id, std::vector<int>& )    = 0;
 virtual              int    SetDouble         (const char* name, uint32_t id, double val)            = 0;
 virtual              int    SetString         (const char* name, uint32_t id, const char* val)       = 0;
 virtual              int    GetInt            (const char* name, uint32_t id, int& val)              = 0;
 virtual              int    GetDouble         (const char* name, uint32_t id, double& val)           = 0;
 virtual              int    GetString         (const char* name, uint32_t id, char* val, int maxlen) = 0;
 virtual              int    GetEndPoint       (const char* name, uint32_t id, struct sockaddr_in&)   = 0;
 virtual              int    SetIntEx          (const char* name,  int val)                           = 0;
 virtual              int    SetDoubleEx       (const char* name,  double val)                        = 0;
 virtual              int    SetStringEx       (const char* name,  const char* val)                   = 0;
 virtual              int    GetIntEx          (const char* name,  int& val)                          = 0;
 virtual              int    GetDoubleEx       (const char* name,  double& val)                       = 0;
 virtual              int    GetStringEx       (const char* name,  char* val, int maxlen)             = 0;
 virtual              int    GetEndPointEx     (const char* name,  struct sockaddr_in&)               = 0;
 virtual              int    GetIntVEx         (const char* name,  std::vector<int>& )                = 0;
 virtual             void    Flush             (void)                                                 = 0;
 };

class Config
   {
     protected:
                          Config         ();
     public:
  static          Config* NewConfig      (void);
  static            void  DestroyConfig  (Config* _conf);
 virtual                 ~Config         ();
 virtual             int  LoadConfig     (const char* fname) = 0;
 virtual             int  SaveConfig     (const char* fname) = 0;
 virtual        uint32_t  GetNumSections (const char* name) = 0;
 virtual  ConfigSection*  GetRoot        (void)              = 0;
   };

}}

