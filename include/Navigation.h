#pragma once

#include <TimeSystem.h>
#include <Proto.h>
#include <netinet/in.h>

namespace slib     {
namespace services {

using namespace slib::proto;

class Navigation
{
  public:

enum NaviMode
  {
    NM_NO_VALID,
    NM_LOCAL,
    NM_REMOTE,
    NM_STREAM
  };

struct NaviInit
           {
            NaviMode m_Mode;
                bool m_StoreAll;
                char m_UnixPath[256];
            uint32_t m_HeartBeatDelay;
  struct sockaddr_in m_NetPath;
     struct timespec m_StorePeriod;
     struct timespec m_StoreInterval;
         TimeSystem* m_TSystem;
           };

protected:

                       Navigation  ( );

   public:

     virtual          ~Navigation           (  );
  static  Navigation*  NewNavigationClient  ( NaviInit& _init_data, bool autostart = true ); 
 virtual          int  SetInitData          ( NaviInit& _init_data, bool restart = true ) = 0;
 virtual          int  Start                ( void )                    = 0;
 virtual         void  Stop                 ( void )                    = 0;
 virtual         bool  IsDataReady          ( struct timespec& _time )  = 0;
 virtual          int  GetPosOnTime         ( struct NaviDataMsg& pos, struct timespec& _time ) = 0;
 virtual          int  GetCurPosition       ( struct NaviDataMsg& pos ) = 0;
};

}
}
