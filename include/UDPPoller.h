#ifndef UPD_POLLER_H
#define UDP_POLLER_H

#include <Common.h>
#include <DataBuffer.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <string>
#include <map> 
#include <vector> 
using namespace std;

namespace slib
{

using namespace common;

 class UDPPoller
   {
       public:

 typedef struct 
               {
                uint32_t sock;
                uint32_t events;     
               } sockEvent_t;

   enum
         {
          EP_IN      = EPOLLIN,
          EP_OUT     = EPOLLOUT,
          EP_PRI     = EPOLLPRI,
          EP_ERR     = EPOLLERR,
          EP_HUP     = EPOLLHUP,
          EP_ET      = EPOLLET,
          EP_ONESHOT = EPOLLONESHOT
         };

     protected:
          struct soc_reg
            {
                int sock;
           uint32_t count; 
            };  
                               int  MaxEvents;
                               int  StartSocCnt; 
                               int  EpollSocket;    
                                    UDPPoller(){};
map<in_addr_t,map<uint16_t,soc_reg> >  RegMap;

        int IsEPPresent (struct sockaddr_in& ep); 
     public:
           UDPPoller(int max_events, int soc_cnt);          
          ~UDPPoller(); 

      int InsertLocalPort(uint16_t port, int events);
      int InsertEndPoint(string& ep_str,uint32_t events);
      int InsertEndPoint(struct sockaddr_in& ep,uint32_t events); 
      int RemoveEndPoint(string& ep_str);
      int RemoveEndPoint(struct sockaddr_in& ep); 
      int PollingStart();
      int PollingStop();   
      int EventWait(vector<sockEvent_t> &result,int timeout=-1);
      int InsertDataBuffer (DataBuffer* buffer);
 DataBuffer* GetDataBuffer    (void);     
   };

}
#endif
