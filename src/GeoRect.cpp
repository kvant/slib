#include <Geo.h>
#include <string.h>

namespace slib {
namespace geo  {

//=====================================================================  
  
LineXYZ::LineXYZ (struct CoordDec3D a, struct CoordDec3D b ){
 m_Pnt[0] = a;
 m_Pnt[1] = b;
 m_K  = (b.Y-a.Y)/(b.X-a.X);
 m_Ko = 1.0/m_K;
 m_B  = b.Y - m_K*b.X; }
  
//=====================================================================  
  
GeoRect::GeoRect        ( struct CoordGeo dots[4] ):m_XYZValid(false) {
 memcpy(m_PointsBLH,dots,(sizeof(struct CoordGeo)*4)); }

//=====================================================================

void   
GeoRect::PrepareXYZ     ( struct CoordGeo& center ) {
 BLH2xyz(center,m_PointsBLH[0],m_PointsXYZ[0]); 
 BLH2xyz(center,m_PointsBLH[1],m_PointsXYZ[1]);
 BLH2xyz(center,m_PointsBLH[2],m_PointsXYZ[2]);
 BLH2xyz(center,m_PointsBLH[3],m_PointsXYZ[3]);
 m_Center = center;
 InitByXYZ(m_PointsXYZ); }     

//=====================================================================

bool   
GeoRect::IsPointInRect  ( struct CoordDec3D& point  ) {
 
  uint ind = 0;
  uint lines[2] = {L_NA,L_NA};
  
  if (!m_XYZValid                || \
       point.X > m_RightTop.X    || \
       point.X < m_LeftBottom.X  || \
       point.Y > m_RightTop.Y    || \
       point.Y < m_LeftBottom.Y ) return false;
  
  if (m_Lines[L_01].iDx(point.X))  lines[ind++] = L_01;
  if (m_Lines[L_12].iDx(point.X))  lines[ind++] = L_12;
  if (m_Lines[L_23].iDx(point.X))  lines[ind++] = L_23;
  if (m_Lines[L_30].iDx(point.X))  lines[ind++] = L_30;
  
  double yv[] = {m_Lines[lines[0]].Y(point.X),m_Lines[lines[1]].Y(point.X)};
  return ( (yv[0]<point.Y && yv[1]>point.Y) || (yv[0]>point.Y && yv[1]<point.Y) );
}

//=====================================================================

void   
GeoRect::InitByXYZ ( struct CoordDec3D dots[4]){
  
 m_Lines[L_01] = LineXYZ(m_PointsXYZ[0],m_PointsXYZ[1]);
 m_Lines[L_12] = LineXYZ(m_PointsXYZ[1],m_PointsXYZ[2]);
 m_Lines[L_23] = LineXYZ(m_PointsXYZ[2],m_PointsXYZ[3]);
 m_Lines[L_30] = LineXYZ(m_PointsXYZ[3],m_PointsXYZ[0]);
 
 m_LeftBottom = m_RightTop = m_PointsXYZ[0];
 
 for (uint a = 0; a<4; a++){
  m_LeftBottom.X = ( m_LeftBottom.X > m_PointsXYZ[a].X )?m_PointsXYZ[a].X:m_LeftBottom.X;
  m_LeftBottom.Y = ( m_LeftBottom.Y > m_PointsXYZ[a].Y )?m_PointsXYZ[a].Y:m_LeftBottom.Y;
  m_RightTop.X   = ( m_RightTop.X < m_PointsXYZ[a].X )?m_PointsXYZ[a].X:m_RightTop.X;
  m_RightTop.Y   = ( m_RightTop.Y < m_PointsXYZ[a].Y )?m_PointsXYZ[a].Y:m_RightTop.Y;
 }
 
 m_XYZValid = true; }

//=====================================================================

bool   
GeoRect::IsPointInRect  ( struct CoordGeo& point, struct CoordGeo& center ) {

}

//=====================================================================

bool   
GeoRect::IsPointInRect  ( struct CoordGeo& point ) {

}

}}
