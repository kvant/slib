#include <UDPPoller.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <vector>

namespace slib{

UDPPoller::UDPPoller(int max_events, int soc_cnt):MaxEvents(max_events),StartSocCnt(soc_cnt)
{
  EpollSocket = epoll_create(soc_cnt);
}

//=================================================================================

UDPPoller::~UDPPoller()
{
 map<in_addr_t,map<uint16_t,soc_reg> >::iterator mm_it = RegMap.begin();
 map<uint16_t,soc_reg>::iterator m_it;
 while (mm_it!=RegMap.end())
   {
     m_it = mm_it->second.begin();
     while(m_it!=mm_it->second.end())
      {
       close(m_it->second.sock);
       ++m_it;  
      }       
    ++mm_it;  
   }
 RegMap.clear();
} 

//=================================================================================
      
int 
UDPPoller::InsertEndPoint(string& ep_str, uint32_t events)
{
 struct sockaddr_in addr;
 if(Str2SockAddr(ep_str,addr)) return -1;
 InsertEndPoint(addr,events);
 return 0;
}
    
//=================================================================================
  
int 
UDPPoller::InsertEndPoint(struct sockaddr_in& ep, uint32_t events)
{
 if (EpollSocket==-1) return -1;
 if (IsEPPresent(ep))
      RemoveEndPoint(ep);

 int new_sock = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP);  
 if (bind(new_sock,(const struct sockaddr*)&ep,sizeof(ep))) 
   {
      perror("Bind");
      close(new_sock);
      return -1;
   }
 
  RegMap[ep.sin_addr.s_addr][ep.sin_port].sock  = new_sock;
  RegMap[ep.sin_addr.s_addr][ep.sin_port].count = 0; 

  struct epoll_event e_ev;
   e_ev.events  = events;
   e_ev.data.fd = new_sock;
  epoll_ctl(EpollSocket,EPOLL_CTL_ADD,new_sock,&e_ev); 

 printf("UDP EP added success\n");  
 return 0;
}
      
//=================================================================================

int 
UDPPoller::RemoveEndPoint(string& ep_str)
{

return 0;
}
      
//=================================================================================

int 
UDPPoller::RemoveEndPoint(struct sockaddr_in& ep)
{

return 0;
}
      
//=================================================================================

int 
UDPPoller::PollingStart()
{

return 0;
}
      
//=================================================================================

int 
UDPPoller::PollingStop()
{

return 0;
}

//=================================================================================
         
int 
UDPPoller::InsertDataBuffer (DataBuffer* buffer)
{

return 0;
}

//=================================================================================

DataBuffer* 
UDPPoller::GetDataBuffer (void)
{
return NULL;
}

//=================================================================================


int 
UDPPoller::IsEPPresent (struct sockaddr_in& ep)
{
 if (RegMap.count(ep.sin_addr.s_addr))
   return RegMap[ep.sin_addr.s_addr].count(ep.sin_port);
   return 0;
}

//=================================================================================

int 
UDPPoller::EventWait(vector<sockEvent_t> &result,int timeout)
{
 result.clear();
 struct epoll_event ev;
 if (epoll_wait(EpollSocket,&ev,1,timeout)==1)
  {
   sockEvent_t r_ev = {ev.data.fd,ev.events};
   result.push_back(r_ev);  
  }
 return 0;
}

//=================================================================================

int 
UDPPoller::InsertLocalPort(uint16_t port, int events)
{
 struct sockaddr_in addr;
 addr.sin_addr.s_addr = 0;  
 addr.sin_port        = htons(port);
 addr.sin_family      = AF_INET;
 return InsertEndPoint(addr,events);
}

//=================================================================================

}
