#include <Proto.h>
#include "string.h"
#include <cstdio>
#include <stdint.h>
#include <stdlib.h>

namespace slib  {
namespace proto {


static const double  cDISTANCE  = 256000.0/double(0x80000000);
static const double     cANGLE  = 180.0/double(0x8000);
static const double cRANGE_RATE = 1000.0/double(0x4000);
static const double cRMS_RATE   = 15000.0/double(0x8000);
static const double     cSPEED  = 1000.0/double(0x4000);
static const double       cXYZ  = 256000.0/double(0x40000000);
static const double       cBLH  = 180.0/double(0x80000000);
static const double    cHEIGHT  = 1e-6;
//======================================================================================================================

void
convData2RawPoint(char* data, struct msgRawPoint& _res, uint32_t& size)
{
 char* cur_ptr = data;
 memset(&_res,0x00,sizeof(_res));

 _res.LocTime.tv_sec       = *((uint32_t*)cur_ptr);                              cur_ptr+=4; //4
 _res.ObjType              = ((*((uint16_t*)cur_ptr))>>OFFSET_LO_TYPE)&0x07;
 _res.MsgType              = (*((uint16_t*)cur_ptr))&0x07;
 _res.MsgFeature           = ((*((uint16_t*)cur_ptr))>>OFFSET_MSG_FEATURE)&0x07; cur_ptr+=2; //6
 _res.ObjSrcNumber         = (*((uint16_t*)cur_ptr))&0x3FFF;
 _res.Nationality          = (*((uint16_t*)cur_ptr))>>OFFSET_LO_NATIONALITY&0xf; cur_ptr+=2; //8
 _res.CoordsSph.Distance   = (*((uint32_t*)cur_ptr))*cDISTANCE;                  cur_ptr+=4; //12
 _res.CoordsSph.Azimuth    = (*((int16_t*)cur_ptr))*cANGLE;                      cur_ptr+=2; //14
 _res.CoordsSph.Elevation  = (*((int16_t*)cur_ptr))*cANGLE;                      cur_ptr+=2; //16
 _res.Vr                   = (*((uint16_t*)cur_ptr))*cRANGE_RATE;                cur_ptr+=2; //18
 _res.SN                   = (*((uint16_t*)cur_ptr));                            cur_ptr+=2; //20
 _res.RMS_dist             = (*((uint16_t*)cur_ptr))*cDISTANCE;                  cur_ptr+=2; //22
 _res.RMS_az               = (*((uint16_t*)cur_ptr))*cANGLE;                     cur_ptr+=2; //24
 _res.RMS_el               = (*((uint16_t*)cur_ptr))*cANGLE;                     cur_ptr+=2; //26


  _res.CoordsSph.Azimuth = DEG_TO_RAD(_res.CoordsSph.Azimuth );
  NORMALIZE_ANGLE(_res.CoordsSph.Azimuth);

  _res.CoordsSph.Elevation = DEG_TO_RAD(_res.CoordsSph.Elevation );
  NORMALIZE_ANGLE(_res.CoordsSph.Elevation);

//remap msg type

 switch(_res.MsgType)
  {
   case MSG_GT_RP_AED:
       _res.MsgType = MSG_T_RAW_POINT;
        break;

   case MSG_GT_BR_AE:
       _res.MsgType = MSG_T_AE_BEARING;
        break;

   case MSG_GT_BR_A:
       _res.MsgType = MSG_T_A_BEARING;
        break;

   case MSG_GT_TP_XYZ:
       _res.MsgType = MSG_T_TRACK_POINT;
        break;

   case MSG_GT_TP_NP:
       _res.MsgType = MSG_T_TRACK_FPOINT;

        break;
  }

 _res.ObjModNumber         = (*((uint16_t*)cur_ptr));                            cur_ptr+=2;
 _res.RMS_az               =  0.06*M_PI/180.0;
 _res.RMS_el               =  0.06*M_PI/180.0;
 _res.RMS_dist             =  5.0;
  size = (cur_ptr-data);
}

//======================================================================================================================

void
convData2TrackPoint(char* data, struct msgTrackPoint& res, uint32_t& size)
{
	char* cur_ptr = data;
	memset(&res, 0x00, sizeof(res));

	res.LocTime.tv_sec = *((uint32_t*)cur_ptr);
	cur_ptr+=4;  //4

	res.ObjType = ((*((uint16_t*)cur_ptr)) >> OFFSET_LO_TYPE) & 0x7;
	res.MsgType = (*((uint16_t*)cur_ptr)) & 0x07;
	res.MsgFeature = ((*((uint16_t*)cur_ptr)) >> OFFSET_MSG_FEATURE) & 0x7;
	cur_ptr+=2;  //6

	res.ObjSrcNumber = (*((uint16_t*)cur_ptr)) & 0x3FFF;
	res.Nationality = ((*((uint16_t*)cur_ptr)) >> OFFSET_LO_NATIONALITY) & 0x3;
	cur_ptr+=2;  //8

	res.CoordsSph.Distance = (*((uint32_t*)cur_ptr))*cDISTANCE;
	cur_ptr+=4;  //12

	res.CoordsSph.Azimuth = (*((uint16_t*)cur_ptr))*cANGLE;
	cur_ptr+=2;  //14

	res.CoordsSph.Elevation = (*((uint16_t*)cur_ptr))*cANGLE;
	cur_ptr+=2;  //16

	res.Vr = (*((int16_t*)cur_ptr))*cRANGE_RATE;
	cur_ptr+=2;  //18

	res.CoordRMS.RMSx = (*((uint16_t*)cur_ptr)); /*400.0;*/
	cur_ptr+=2;  //20

	res.CoordRMS.RMSy = (*((uint16_t*)cur_ptr)); /*400.0;*/
	cur_ptr+=2;  //22

	res.CoordRMS.RMSz = (*((uint16_t*)cur_ptr)); /*1000.0;*/
	cur_ptr+=2;  //24

	res.VelRMS.RMSx = (*((uint16_t*)cur_ptr)); /*400.0;*/
	cur_ptr+=2;  //26

	res.VelRMS.RMSy = (*((uint16_t*)cur_ptr)); /*400.0;*/
	cur_ptr+=2;  //28

	res.VelRMS.RMSz = (*((uint16_t*)cur_ptr)); /*400.0;*/
	cur_ptr+=2;  //30

	res.V.Vx = (*((int16_t*)cur_ptr))*cSPEED;
	cur_ptr+=2;  //32

	res.V.Vy = (*((int16_t*)cur_ptr))*cSPEED;
	cur_ptr+=2;  //34

	res.V.Vz = (*((int16_t*)cur_ptr))*cSPEED;
	cur_ptr+=2;  //36

	res.CoordsDec.X = (*((int32_t*)cur_ptr))*cXYZ;
	cur_ptr+=4;  //40

	res.CoordsDec.Y = (*((int32_t*)cur_ptr))*cXYZ;
	cur_ptr+=4;  //44

	res.CoordsDec.Z = (*((int32_t*)cur_ptr))*cXYZ;
	cur_ptr+=4;  //48

	//remap msg type

	switch(res.MsgType) {
	case MSG_GT_RP_AED:
	     res.MsgType = MSG_T_TRACK_POINT;
	     break;

	case MSG_GT_BR_AE:
	     res.MsgType = MSG_T_AE_BEARING;
	     break;

	case MSG_GT_BR_A:
	     res.MsgType = MSG_T_A_BEARING;
	     break;
	case MSG_GT_TP_XYZ:
	     res.MsgType = MSG_T_TRACK_POINT;
	     break;

	case MSG_GT_TP_NP:
	     res.MsgType = MSG_T_TRACK_FPOINT;
	     break;
	}

	res.ObjModNumber = *((uint16_t*)cur_ptr);
	cur_ptr += 4;  //52

	size = cur_ptr - data;
}

//======================================================================================================================

void
convData2Frame(char* data, std::vector<struct msgFrame>& resv)
{
	struct msgFrame res;

	resv.clear();
	memset(&res.Header, 0x00, sizeof(res.Header));
	res.RawPoints.clear();
	res.TrackPoints.clear();

	char* cur_ptr = data;
	uint32_t numMsgsRT;
	uint32_t numMsgsTT;
	uint32_t msgSize;
	struct msgRawPoint mRP;
	struct msgTrackPoint mTP;

	res.Header.Time =   *((uint32_t*)cur_ptr);
	cur_ptr += 4;

	res.Header.Enabled = ((*((uint16_t*)cur_ptr)) >> OFFSET_SRC_ENABLE) & 0x1;
	res.Header.Worked = ((*((uint16_t*)cur_ptr)) >> OFFSET_SRC_WORKED) & 0x1;
	res.Header.WorkMode = ((*((uint16_t*)cur_ptr)) >> OFFSET_SRC_WORK_MODE) & 0x3;
	res.Header.UpdateTimes = ((*((uint16_t*)cur_ptr)) >> OFFSET_SRC_UPDATE_TIME) & 0x1f;
	res.Header.EnabledZones = (*((uint16_t*)cur_ptr)) & 0x1f;
	cur_ptr += 2;

	res.Header.Sector[0] = ((uint8_t*)cur_ptr)[0];
	res.Header.Sector[1] = ((uint8_t*)cur_ptr)[1];
	cur_ptr += 2;

	numMsgsRT = (*((uint16_t*)cur_ptr)) & 0xff;
	numMsgsTT = ((*((uint16_t*)cur_ptr)) >> 8) & 0xff;
	cur_ptr += 2;

#if 0
	if (numMsgsRT||numMsgsTT)  printf("NumTP = %d NumRP = %d\n",numMsgsTT,numMsgsRT);
#endif

	for(uint32_t i = 0; i < numMsgsRT; i++) {
		convData2RawPoint(cur_ptr, mRP, msgSize);
		res.RawPoints.push_back(mRP);
		cur_ptr += msgSize;
	}

	for(uint32_t i = 0; i < numMsgsTT; i++) {
		convData2TrackPoint(cur_ptr, mTP, msgSize);
		res.TrackPoints.push_back(mTP);
		cur_ptr += msgSize;
	}

	resv.push_back(res);
}

//======================================================================================================================

void
convData2MFrame  ( char* data, std::vector<struct msgFrame>& _resv, uint nsrc )
{
       char* carr = data;
   uint16_t* warr = (uint16_t*)data;
  uint16_t tw;
  uint ndots;
  _resv.clear();
  uint32_t send_time = *((uint32_t*)data);
  carr = (char*) &(warr[nsrc*2+2]);

  struct msgFrame tframe;

  for (uint ns=0; ns<nsrc; ns++)
   {
        tw =  warr[2+ns*2];
     ndots = (warr[3+ns*2]>>8)&0xFF;
     struct srcInfo hdr = {
                          SrcId:(warr[3+ns*2]&0xFF),
                           Time:send_time,
                        Enabled:(bool)((tw >> 15) & 1),
                         Worked:(bool)((tw >> 14) & 1),
                        VrValid:(bool)((tw >> 11) & 1),
                       WorkMode:(uint8_t)((tw >> 12) & 3),
                    UpdateTimes:(uint8_t)((tw >> 5) & 0x1F),
                   EnabledZones:(uint8_t)(tw & 0x1F)
                          };

     tframe.Header = hdr;
     tframe.TrackPoints.clear();
     tframe.RawPoints.clear();

     while(ndots--)
      {
                       uint  tp_size;
                      uint*  dwarr = (uint*) carr;
       struct msgTrackPoint  tr_p;
       convData2TrackPoint(carr+12,tr_p,tp_size);
       tr_p.RefPoint.B = DEG_TO_RAD(dwarr[0]*cBLH);
       tr_p.RefPoint.L = DEG_TO_RAD(dwarr[1]*cBLH);
       tr_p.RefPoint.H = dwarr[2]*cHEIGHT;
       tframe.TrackPoints.push_back(tr_p);
       carr += (tp_size + 12);
      }
    _resv.push_back(tframe);
   }
}

//======================================================================================================================

void
convFrame2Data (struct msgFrame& _src, char* data, int max_size, size_t& size)
{
 char* cur_ptr = data;
 uint16_t tmp_w;
 uint32_t cnt;
   size_t sz;

 *((uint32_t*)cur_ptr) = _src.Header.Time;
  cur_ptr+=4;

 tmp_w = ((_src.Header.Enabled?(1<<OFFSET_SRC_ENABLE):0) |
          (_src.Header.Worked?(1<<OFFSET_SRC_WORKED):0)  |
          ((_src.Header.WorkMode&0x3)<<OFFSET_SRC_WORK_MODE) |
          ((_src.Header.UpdateTimes&0x1f)<<OFFSET_SRC_UPDATE_TIME) |
          (_src.Header.EnabledZones&0x1f));
  *((uint16_t*)cur_ptr) = tmp_w;
  cur_ptr+=2;

 ((uint8_t*)cur_ptr)[0] = _src.Header.Sector[0];
 ((uint8_t*)cur_ptr)[1] = _src.Header.Sector[1];
 cur_ptr+=2;

  tmp_w = (((_src.TrackPoints.size()<<8)&0xFF00)|((_src.RawPoints.size())&0xFF));
  *((uint16_t*)cur_ptr) = tmp_w;
  cur_ptr+=2;

  cnt = _src.RawPoints.size();
  for (uint32_t i=0;i<cnt;i++)
    {
      convRawPoint2Data(_src.RawPoints[i],cur_ptr,sz);
      cur_ptr+=sz;
    }

  cnt = _src.TrackPoints.size();
  //printf("======> In frame %d track points\n",cnt);
  for (uint32_t i=0;i<cnt;i++)
    {
      convTrackPoint2Data(_src.TrackPoints[i],cur_ptr,sz);
      cur_ptr+=sz;
    }
 size = cur_ptr-data;
}


//======================================================================================================================

void
convFrame2DataCons (struct msgFrame& _src, char* data, int max_size, size_t& size)
{
 char* cur_ptr = data;
 uint16_t tmp_w;
 uint32_t cnt;
   size_t sz;

 *((uint32_t*)cur_ptr) = _src.Header.Time;
  cur_ptr+=4;

  tmp_w = (((_src.TrackPoints.size()<<8)&0xFF00) | ((_src.RawPoints.size())&0xFF));
  *((uint16_t*)cur_ptr) = tmp_w;
  cur_ptr+=2;

  *((uint16_t*)cur_ptr) = 1;
  cur_ptr+=2;

  cnt = _src.RawPoints.size();
  for (uint32_t i=0;i<cnt;i++)
    {
      convRawPoint2Data(_src.RawPoints[i],cur_ptr,sz);
      cur_ptr+=sz;
    }

  cnt = _src.TrackPoints.size();
  for (uint32_t i=0;i<cnt;i++)
    {
      convTrackPoint2Data(_src.TrackPoints[i],cur_ptr,sz);
      cur_ptr+=sz;
    }

size = cur_ptr-data;
}

//================================================================================================

void convTrackPoint2Data (struct msgTrackPoint &_src, char *data, size_t &size)
{
	char* cur_ptr = data;

	*((uint32_t*)cur_ptr) = _src.LocTime.tv_sec;
		cur_ptr+=4; // 4

	*((uint16_t*)cur_ptr) = (((_src.ObjType & 0x7) << OFFSET_LO_TYPE) |
			((_src.MsgFeature & 0xE) << OFFSET_MSG_FEATURE) |
			(_src.MsgType & 0x07));
		cur_ptr+=2; // 6

	*((uint16_t*)cur_ptr) = (((_src.Nationality & 0x3) << OFFSET_LO_NATIONALITY) |
			(_src.ObjSrcNumber&0x3FFF));
		cur_ptr+=2; //8

	*((uint32_t*)cur_ptr) = (uint32_t)(_src.CoordsSph.Distance / cDISTANCE);
		cur_ptr+=4; //12
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.CoordsSph.Azimuth / cANGLE);
		cur_ptr+=2; //14
	*((int16_t*)cur_ptr) = (int16_t)(_src.CoordsSph.Elevation / cANGLE);
		cur_ptr+=2; //16
	*((int16_t*)cur_ptr) = (int16_t)(_src.Vr / cRANGE_RATE);
		cur_ptr+=2; //18
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.CoordRMS.RMSx / cRMS_RATE);
		cur_ptr+=2; //20
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.CoordRMS.RMSy / cRMS_RATE);
		cur_ptr+=2; //22
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.CoordRMS.RMSz / cRMS_RATE);
		cur_ptr+=2; //24
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.VelRMS.RMSx / cSPEED);
		cur_ptr+=2; //26
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.VelRMS.RMSy / cSPEED);
		cur_ptr+=2; //28
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.VelRMS.RMSz / cSPEED);
		cur_ptr+=2; //30
	*((int16_t*)cur_ptr) = (int16_t)(_src.V.Vx / cSPEED);
		cur_ptr+=2; //32
	*((int16_t*)cur_ptr) = (int16_t)(_src.V.Vy / cSPEED);
		cur_ptr+=2; //34
	*((int16_t*)cur_ptr) = (int16_t)(_src.V.Vz / cSPEED);
		cur_ptr+=2; //36
	*((int32_t*)cur_ptr) = (int32_t)(_src.CoordsDec.X / cXYZ);
		cur_ptr+=4; //40
	*((int32_t*)cur_ptr) = (int32_t)(_src.CoordsDec.Y / cXYZ);
		cur_ptr+=4; //44
	*((int32_t*)cur_ptr) = (int32_t)(_src.CoordsDec.Z / cXYZ);
		cur_ptr+=4; //48
	*((uint16_t*)cur_ptr) = (uint16_t)(_src.ObjModNumber);
		cur_ptr+=4; //26

	size = cur_ptr-data;
}

//================================================================================================

void
convRawPoint2Data (struct msgRawPoint& _src,char* data, size_t& size)
{
char* cur_ptr = data;

*((uint32_t*)cur_ptr) = _src.LocTime.tv_sec;                                                                  cur_ptr+=4; //4
*((uint16_t*)cur_ptr) = (((_src.ObjType&0x7)<<OFFSET_LO_TYPE) | ((_src.MsgFeature&0xE)<<OFFSET_MSG_FEATURE)); cur_ptr+=2; //6
*((uint16_t*)cur_ptr) = (((_src.Nationality&0xC)<<OFFSET_LO_NATIONALITY) | (_src.ObjSrcNumber&0x3FFF));       cur_ptr+=2; //8

*((uint32_t*)cur_ptr) = (uint32_t)(_src.CoordsSph.Distance/cDISTANCE);                                        cur_ptr+=4; //12
*((int16_t*)cur_ptr)  = (int16_t)(_src.CoordsSph.Azimuth/cANGLE);                                             cur_ptr+=2; //14
*((int16_t*)cur_ptr)  = (int16_t)(_src.CoordsSph.Elevation/cANGLE);                                           cur_ptr+=2; //16
*((int16_t*)cur_ptr)  = (int16_t)(_src.Vr/cRANGE_RATE);                                                       cur_ptr+=2; //18
*((int16_t*)cur_ptr)  = _src.SN;                                                                              cur_ptr+=2; //20
*((uint16_t*)cur_ptr) = (uint16_t)(_src.RMS_dist/cDISTANCE);                                                  cur_ptr+=2; //22
*((uint16_t*)cur_ptr) = (uint16_t)(_src.RMS_az/cANGLE);                                                       cur_ptr+=2; //24
*((uint16_t*)cur_ptr) = (uint16_t)(_src.RMS_el/cANGLE);                                                       cur_ptr+=2; //26
*((uint16_t*)cur_ptr) = (uint16_t)(_src.ObjModNumber);                                                        cur_ptr+=2; //26
size = cur_ptr-data;
}

//================================================================================================

void convRaw2SAMSMgs        ( void* data, struct msgSAMS2TDS& msg, uint32_t& size, uint8_t sams_id, uint8_t ch_id)
{
 memset(&msg,0x00,sizeof msg);
 msg.m_SamsId     = sams_id;
 msg.m_ChanId     = ch_id;
 msg.m_State      = ((uint16_t*)data)[0]>>10;
 msg.m_Ammunition = ((uint16_t*)data)[0] & 0x1FF;
 msg.m_TargCoords.Distance   =  ((uint32_t)(((uint16_t*)data)[1]) + ((uint32_t)(((uint16_t*)data)[2])<<16))*cDISTANCE;
 msg.m_TargCoords.Azimuth    =  DEG_TO_RAD(((uint16_t*)data)[3]*cANGLE);
 msg.m_TargCoords.Elevation  =  DEG_TO_RAD(((int16_t*)data)[4]*cANGLE);

#if 0
 printf("Channel %d GCU in %d ammo(%d) \n",msg.m_ChanId, IS_READY4TA(msg.m_State), msg.m_Ammunition);
 printf("D(%g) A(%g) E(%g) \n",msg.m_TargCoords.Distance, RAD_TO_DEG(msg.m_TargCoords.Azimuth), RAD_TO_DEG(msg.m_TargCoords.Elevation));
#endif

 size = WORDS_IN_SAMS_MSG*sizeof(uint16_t);
}

//================================================================================================

void convTDSMsf2Raw         ( struct msgTDS2SAMS& msg, void* data, uint32_t& size)
{

}

//================================================================================================

void convRaw2SAMSFrame      ( void* raw_frame, struct frmSAMS2TDS& frame, uint8_t sams_id , struct timespec& tm )
{

 frame.m_ChanData.clear();
 frame.m_SId = sams_id;
 uint16_t* wdata = static_cast<uint16_t*>(raw_frame);
 uint8_t  nc  = (wdata[0]>>OFFSET_NUM_CHAN);
 uint8_t  c = 0;
 uint32_t sz;
 struct msgSAMS2TDS msg;
 //printf("Num channels = %d\n",nc);
 if (!nc) return;
 wdata+=WORDS_IN_SAMS_HDR;
 do
  {
   convRaw2SAMSMgs(wdata,msg,sz,sams_id,c);
   msg.m_Time = tm;
   frame.m_ChanData.push_back(msg);
   wdata += (sz>>1);
  }while(++c^nc);
}

//================================================================================================

void
convSAMSFrame2Raw      ( struct frmSAMS2TDS& frame, uint32_t tv_usec, void* data, uint32_t max_size, uint32_t& r_size) {

}

//================================================================================================

static
__inline__
struct msgRawPoint&
msgSAMS2msgSRC ( struct msgSAMS2TDS& msg_a ) {
 static struct msgRawPoint rp;

 rp.ObjType         = LO_TYPE_AIRCRAFT;
 rp.MsgFeature      = (IS_TRG_AIMED(msg_a.m_State))?MSG_F_NEXT_LOCATION:MSG_F_TRACK_DROPED;
 rp.MsgType         = MSG_T_RAW_POINT;
 rp.ObjSrcNumber    = msg_a.m_ChanId;
 rp.ObjModNumber    = -1;
 rp.Nationality     = LO_NF_UNKNOWN;
 rp.LocTime         = msg_a.m_Time;
 rp.CoordsSph       = msg_a.m_TargCoords;
 rp.Vr              = 0;
 rp.SN              = 0;
 rp.RMS_az          = DEG_TO_RAD((2.0/60.0));
 rp.RMS_el          = DEG_TO_RAD((2.0/60.0));
 rp.RMS_dist        = 10;

 return rp; }


//================================================================================================

void
convSAMSFrame2Frame ( struct frmSAMS2TDS& frame, std::vector<struct msgFrame>& _res ) {
  _res.clear();
  uint sz = frame.m_ChanData.size();
  struct msgFrame fr;
  _res.push_back(fr);
  _res[0].Header.SrcId = frame.m_SId;
  for (uint a=0; a<sz; _res[0].RawPoints.push_back(msgSAMS2msgSRC(frame.m_ChanData[a++])) ); }

//================================================================================================

void
convCONSFrame2Raw ( struct frmTDS2CONS& frame, void* data, uint32_t max_size, uint32_t& rsize)
{
uint16_t* warr = (uint16_t*)data;
uint16_t* cur_w;
warr[0] = frame.m_TimeUsec&0xFF;
warr[1] = (frame.m_TimeUsec>>16)&0xFF;
warr[2] = (((frame.m_MsgVec.size()&0x1F)<<11) | frame.m_Id);
warr[3] = 2;

cur_w = &warr[4];

for(uint a = 0; a < frame.m_MsgVec.size(); a++ ) {
 cur_w[0] = (((frame.m_MsgVec[a].m_State & 0x3F) << 10) | (frame.m_MsgVec[a].m_Ammo & 0x1FF));
 cur_w[1] = frame.m_MsgVec[a].m_TNumber;
 cur_w+=2; }

 rsize = frame.m_MsgVec.size()*4+8;
}

//================================================================================================
}
}
