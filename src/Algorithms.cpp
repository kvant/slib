#include <Algorithms.h>
#include <cstdlib>
#include <cstdio>

namespace      slib {
namespace algorithms {

//=========================================================================================

DMatrix::DMatrix():Rows(0),Columns(0),data(NULL),dptr(NULL)
{
}

void
DMatrix::Init(uint32_t _r,uint32_t _c)             
{
	register uint32_t c = _r-1;
	//register uint32_t k = sizeof(double)*_c;

	if (!( _r && _c))
		return;

	if (data) {
		free (data);
		free (dptr);
	}
///	dptr = (char*) calloc(_r * _c, sizeof(double));
	data = (double**) calloc(_r, sizeof(double*));
	ERows = std::vector<uint32_t>(_r, 0);
	EColumns = std::vector<uint32_t>(_c, 0);
	sERows = std::vector<uint32_t>(_r, 0);
	sEColumns = std::vector<uint32_t>(_c, 0);
	do {
		data[c] = (double*) calloc(_c, sizeof(double));//(double*)(dptr+c*k);
		ERows[c] = c;
	} while(c--);
	c = _c - 1;
	do {
		EColumns[c] = c;
	} while(c--);
}

//=========================================================================================

void
DMatrix::Init(uint32_t _r, uint32_t _c, double _def_val)
{
	register unsigned c = _r - 1;
	register unsigned r;
	//register uint32_t k = sizeof(double)*_c;
	Rows = _r;
	Columns = _c;
	if (!(_r && _c))
		return;

	if (data) {
		free (data);
		free (dptr);
	}
//	dptr = (char*) calloc(_r * _c, sizeof(double));
	data = (double**) new double *[_r];//(_r, sizeof(double*));
	ERows = std::vector<unsigned>(_r, 0);
	EColumns = std::vector<unsigned>(_c, 0);
	sERows = std::vector<uint32_t>(_r, 0);
	sEColumns = std::vector<uint32_t>(_c, 0);
	do {
		data[c] = new double[_c];//(double*) calloc(_c, sizeof(double)); //(double*)(dptr + c * k);
	   	ERows[c] = c;
		for (r=_c; r ; data[c][--r] = _def_val);
	} while(c--);
	c = _c - 1;
	do {
		EColumns[c] = c;
	} while(c--);
}

//=========================================================================================

DMatrix::DMatrix(uint32_t _r,uint32_t _c):Rows(_r),Columns(_c)
{
	Init(_r, _c);
}

//=========================================================================================

DMatrix::~DMatrix()
{
	if (data) {
		for (unsigned i = Rows; i; delete data[--i]);
		delete data;
	}
}

//=========================================================================================
using std::vector;
using std::pair;
void 
dpComputeByRows (struct DMatrix& matrix, struct MatrixDecision& decision)
{

	uint32_t CRow;
	uint32_t CCol;
	uint32_t ColEnd;
	uint32_t RowEnd;
	double Min_sec = 0.0;
	double Min_fst;
	double MRisk_v;

	vector<uint32_t>::iterator Col_it;
	vector<uint32_t>::iterator N;

	vector<uint32_t>::iterator Row_it;
	vector<uint32_t>::iterator Row_its;
	vector<uint32_t>::iterator Col_its;

	decision.Conformance.clear();
	decision.Result = 0;
	while(!(matrix.ERows.empty() || matrix.EColumns.empty())) {
		Row_it  = matrix.ERows.begin();
		RowEnd  = matrix.ERows.back();
		ColEnd  = matrix.EColumns.back();
		MRisk_v = 0.0;
		do {
			CRow = *Row_it;
			Col_it = matrix.EColumns.begin();
			Min_fst = Min_sec = INFINITE_RANGE;
			do {
				CCol =* Col_it;
//				fprintf(stderr, "r=%d c=%d \n", CRow, CCol);
				if (matrix.data[CRow][CCol] < Min_fst) {
					Min_sec = Min_fst;
					Min_fst = matrix.data[CRow][CCol];
					Col_its = Col_it;
				}
				++Col_it;
			} while(CCol ^ ColEnd);
			if ((Min_sec -= Min_fst) >= MRisk_v) {
				N = Col_its;
				Row_its = Row_it;
				MRisk_v = Min_sec;
			}
			++Row_it;
		} while(CRow ^ RowEnd);
		if (MRisk_v == 0.0)
			return;
		decision.Conformance.push_back(pair<uint32_t,uint32_t>(*Row_its, *N));
		decision.Result += matrix.data[*Row_its][*N];
		matrix.ERows.erase(Row_its);
		matrix.EColumns.erase(N);
	}
}
//=========================================================================================

void 
dpComputeByColumns (struct DMatrix& matrix, struct MatrixDecision& decision)
{
   register uint32_t  CRow;
   register uint32_t  CCol;   
   register uint32_t  ColEnd;   
   register uint32_t  RowEnd;     
   register   double  Min_sec = 0.0;
              double  Min_fst;
              double  MRisk_v;

 register  vector<uint32_t>::iterator  Col_it;
           vector<uint32_t>::iterator  N;

   vector<uint32_t>::iterator  Row_it;
   vector<uint32_t>::iterator  Row_its;
   vector<uint32_t>::iterator  Col_its; 
 
 decision.Conformance.clear();
 decision.Result = 0;
 while (!(matrix.ERows.empty()||matrix.EColumns.empty()))
   {
       Col_it = matrix.EColumns.begin(); 
       ColEnd = matrix.EColumns.back();
      RowEnd  = matrix.ERows.back();       
      MRisk_v = 0.0;
      do {
         Row_it  =  matrix.ERows.begin();  
           CCol  = *Col_it;      
         Min_fst = Min_sec = INFINITE_RANGE;      
         do {
             CRow   = *Row_it;           
            if (matrix.data[CRow][CCol]<Min_fst)
              {
               Min_sec = Min_fst;      
               Min_fst = matrix.data[CRow][CCol];
               Row_its = Row_it;
              }  
            ++Row_it; 
           }while(CRow!=RowEnd);   
         if((Min_sec-=Min_fst)>=MRisk_v){
                  N = Row_its;
            Col_its = Col_it;
            MRisk_v = Min_sec; }
          ++Col_it; 
       }while(CCol!=ColEnd);
      if(MRisk_v==0.0) return;
      decision.Conformance.push_back(pair<uint32_t,uint32_t>(*N,*Col_its));
      decision.Result += matrix.data[*N][*Col_its];
      matrix.ERows.erase(N);
      matrix.EColumns.erase(Col_its);
   }
}

//=========================================================================================

void dpDumpMatrix(struct DMatrix& _m)
 {
   uint32_t cnt_r = _m.ERows.size();
   uint32_t cnt_c = _m.EColumns.size();

   if (!(cnt_r&&cnt_c)) return;   
  
   printf("\n");  
   printf("   ");
   for (uint32_t c=0;c<cnt_c;c++)
   printf("         %03d",_m.EColumns[c]);
    printf("\n"); 
   for (uint32_t r=0;r<cnt_r;r++){
     printf("%03d  ",_m.ERows[r]);  
         for (uint32_t c=0;c<cnt_c;c++){
           printf("   %7g  ",(_m.data[_m.ERows[r]][_m.EColumns[c]]==INFINITE_RANGE)?1.0:(_m.data[_m.ERows[r]][_m.EColumns[c]]));
         }
     printf("\n");    
    }   
 }

void DMatrix::SaveRC(void)
{
       sERows = ERows;
       sEColumns = EColumns;
}

void DMatrix::RestoreRC(void)
{
	ERows = sERows;
       EColumns = sEColumns;
}

}}
