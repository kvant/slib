#include <cstring>
#include <Navigation.h>
#include <TimeSupport.h>
#include <TimeSystem.h>
#include <Proto.h>
#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>

namespace slib     {
namespace services {

using namespace slib::timesupport;
using namespace slib::proto;

class NavigationPrivate: public Navigation
   {
#define OLD_TIME TimePeriod[0]
#define LST_TIME TimePeriod[1]
      protected:

               struct NDListItem
                                {
                                           NaviDataMsg  Data;
                                            NDListItem* Next;
                                            NDListItem* Prev;
                                };

                              int   SelfSocket;
                         uint32_t   ItemsInBuff;
                         uint32_t   MaxItems;
                        pthread_t   MainThread;
                        pthread_t   HBThread;
                  pthread_mutex_t   SelfMutex;
                  struct NaviInit   InitData;
                  struct timespec   TimePeriod[2];
               struct NDListItem*   CurItem;
               struct NDListItem*   ItemsList;
                      TimeSystem*   TSystem;

     void __inline__ InsertItem (struct NaviDataMsg& data)
      {
        pthread_mutex_lock(&SelfMutex);
               LST_TIME = data.Time;
               OLD_TIME = ((ItemsInBuff^MaxItems)?OLD_TIME:CurItem->Next->Data.Time);
                CurItem = CurItem->Next;
          CurItem->Data = data;
        pthread_mutex_unlock(&SelfMutex);
      }

 static void*  UNIXThread        (void*);
 static void*  StreamThread      (void*);
 static void*  UDPThread         (void*);
 static void*  HeartbeatThread   (void*);
          int  MakeLocalConnect  (void);
          int  MakeNetConnect  (void);
          int  InitBuffer        (void);
          int  UDPPrepare        (void);
         void  CloseConnects     (void);

     public:

               NavigationPrivate (NaviInit& _init_data, bool autostart);
              ~NavigationPrivate ( );
 virtual  int  SetInitData       (NaviInit& _init_data, bool restart = true);
 virtual  int  Start             (void);
 virtual void  Stop              (void);
 virtual  int  GetPosOnTime      (struct NaviDataMsg& pos, struct timespec& _time);
 virtual  int  GetCurPosition    (struct NaviDataMsg& pos);
 virtual bool  IsDataReady       ( struct timespec& _time );
   };

//=========================================================================================================================================================================


NavigationPrivate::NavigationPrivate(NaviInit& _init_data, bool autostart):InitData(_init_data),TSystem(_init_data.m_TSystem)
{
    pthread_mutex_init(&SelfMutex,NULL);
    memset(&TimePeriod,0x00,sizeof(TimePeriod));
    InitBuffer();
    SetInitData(_init_data,autostart);
}

//=========================================================================================================================================================================

NavigationPrivate::~NavigationPrivate()
{
  pthread_mutex_lock(&SelfMutex);
  if(HBThread)
  pthread_cancel(HBThread);
  if(MainThread)
  pthread_cancel(MainThread);
  pthread_mutex_destroy(&SelfMutex);
  free(ItemsList);
  close(SelfSocket);
}

//=========================================================================================================================================================================

int
NavigationPrivate::SetInitData(NaviInit& _init_data, bool restart)
{
 InitData = _init_data;
 return (restart?Start():0);
}

//=========================================================================================================================================================================

int
NavigationPrivate::InitBuffer()
{
   MaxItems  = (int)ceil(InitData.m_StorePeriod/InitData.m_StoreInterval);
   ItemsList = (struct NDListItem*) calloc(MaxItems,sizeof(struct NDListItem));

    for(uint32_t i=1;i<(MaxItems-1);i++){
        ItemsList[i].Next = &ItemsList[i+1];
        ItemsList[i].Prev = &ItemsList[i-1];}

    ItemsList[0].Next          = &ItemsList[1];
    ItemsList[0].Prev          = &ItemsList[MaxItems-1];
    ItemsList[MaxItems-1].Next = &ItemsList[0];
    ItemsList[MaxItems-1].Prev = &ItemsList[MaxItems-2];

    CurItem = ItemsList;

    ItemsInBuff = 0;

 return 0;
}

//=================================================================================================================================================================

int
NavigationPrivate::UDPPrepare (void)
{
  pthread_attr_t attr;
  CloseConnects();
  SelfSocket = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP);
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
  pthread_create(&HBThread,&attr,HeartbeatThread,this);
  return 0;
}

//=================================================================================================================================================================

int
NavigationPrivate::Start()
{
  Stop();
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
  if (InitData.m_Mode == NM_REMOTE) UDPPrepare();
  pthread_create(&MainThread,&attr,((InitData.m_Mode == NM_REMOTE)?UDPThread:((InitData.m_Mode == NM_STREAM)?StreamThread:UNIXThread)),this);
  return 0;
}

//=================================================================================================================================================================

void
NavigationPrivate::Stop()
{
 if (MainThread) {
  pthread_mutex_lock(&SelfMutex);
  pthread_cancel(MainThread);
  MainThread = 0;
  pthread_mutex_unlock(&SelfMutex);}

 if (HBThread){
  pthread_mutex_lock(&SelfMutex);
  pthread_cancel(HBThread);
  HBThread = 0;
  pthread_mutex_unlock(&SelfMutex);}

  close(SelfSocket);
  SelfSocket = -1;
}

//=================================================================================================================================================================

void
NavigationPrivate::CloseConnects (void)
{
  if(SelfSocket!=-1){
   close(SelfSocket);
   SelfSocket = -1;}
}

//=================================================================================================================================================================

int
NavigationPrivate::MakeLocalConnect(void)
{
 struct sockaddr_un addr;
 if((SelfSocket = socket(PF_UNIX,SOCK_SEQPACKET,0))==-1) return -1;
 fcntl(SelfSocket,F_SETFD,O_NONBLOCK);

 memset(&addr,0x00,sizeof(addr));

 addr.sun_family = AF_UNIX;
 strcpy(addr.sun_path,InitData.m_UnixPath);

 while(connect(SelfSocket,(struct sockaddr*)&addr,sizeof(addr))) {
   fprintf(stderr, "[NAV]: Reconnecting to navigation service\n");
   sleep(1);}

 return 0;
}

//=================================================================================================================================================================

int
NavigationPrivate::MakeNetConnect(void)
{
 if((SelfSocket = socket(PF_INET,SOCK_STREAM,IPPROTO_SCTP))==-1) return -1;
 fcntl(SelfSocket,F_SETFD,O_NONBLOCK);

 printf("NTry to connect to sctp socket: %s:%d\n",inet_ntoa(InitData.m_NetPath.sin_addr),htons(InitData.m_NetPath.sin_port));
  InitData.m_NetPath.sin_family = AF_INET;
 while(connect(SelfSocket,(struct sockaddr*)&InitData.m_NetPath,sizeof(InitData.m_NetPath))) {
   fprintf(stderr, "[NAV]: Reconnecting to navigation service\n");
   sleep(1);}

 return 0;
}

//=================================================================================================================================================================

void*
NavigationPrivate::HeartbeatThread(void* arg)
{
  NavigationPrivate* host = static_cast<NavigationPrivate*>(arg);
  struct NaviConfirm confirm = {CR_GET_DATA_4_ME};
  struct sockaddr_in addr = host->InitData.m_NetPath;
  while(1){
  sendto(host->SelfSocket,&confirm,sizeof(confirm),0,(struct sockaddr*)&addr,sizeof(addr));
  usleep(host->InitData.m_HeartBeatDelay);}
  return NULL;
}

//=================================================================================================================================================================

void*
NavigationPrivate::UDPThread(void* arg)
{

 return NULL;
}

//=================================================================================================================================================================

void*
NavigationPrivate::UNIXThread(void* arg)
{
 pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
 pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);

 NavigationPrivate* host = static_cast<NavigationPrivate*>(arg);

 struct timespec next_time;
 struct NaviDataMsg prev;
 struct NaviDataMsg rcvd;
 int lsock;
 int  nd_size = sizeof(struct NaviDataMsg);

  while(1)
   {
    if(host->MakeLocalConnect()) host->Stop();
    lsock = host->SelfSocket;
    // init by first data

    if(recv(lsock,&rcvd,nd_size,0)!=nd_size) goto con_error;

     host->OLD_TIME = rcvd.Time;
     host->InsertItem(rcvd);
   ++host->ItemsInBuff;
     next_time = rcvd.Time+host->InitData.m_StoreInterval;

 // loop processing
    while(1)
    {
      if(recv(lsock,&rcvd,nd_size,0)!=nd_size) goto con_error;

      if (rcvd.Time>next_time){
         host->InsertItem(((rcvd.Time-next_time)<(next_time-prev.Time))?rcvd:prev);
         next_time+=host->InitData.m_StoreInterval;
         host->ItemsInBuff += (host->ItemsInBuff^host->MaxItems)?1:0;}
         prev = rcvd;}

con_error:
    close(host->SelfSocket);
    host->SelfSocket = -1;
 }
 return NULL;
}

//==============================================================================================================================================================

void*
NavigationPrivate::StreamThread(void* arg)
{
 pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
 pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);

 NavigationPrivate* host = static_cast<NavigationPrivate*>(arg);

 struct timespec next_time;
 struct timepascal rtime_pas;
 struct NaviDataMsg prev;
 struct NaviDataMsg rcvd;

 int lsock;
 int  nd_size = sizeof(struct NaviDataMsg);

  while(1)
   {
    if(host->MakeNetConnect()) host->Stop();
    lsock = host->SelfSocket;
    // init by first data

    if(recv(lsock,&rcvd,nd_size,0)!=nd_size) goto con_error;

     rtime_pas.tp_day  = rcvd.Time.tv_sec;
     rtime_pas.tp_usec = rcvd.Time.tv_nsec;

     tpascal2tspec(rtime_pas,rcvd.Time);

     host->OLD_TIME = rcvd.Time;
     host->InsertItem(rcvd);
   ++host->ItemsInBuff;
     next_time = rcvd.Time+host->InitData.m_StoreInterval;

 // loop processing
    while(1)
    {
      if(recv(lsock,&rcvd,nd_size,0)!=nd_size) goto con_error;
        rtime_pas.tp_day  = rcvd.Time.tv_sec;
        rtime_pas.tp_usec = rcvd.Time.tv_nsec;
        tpascal2tspec(rtime_pas,rcvd.Time);
      if (rcvd.Time>next_time){
         host->InsertItem(((rcvd.Time-next_time)<(next_time-prev.Time))?rcvd:prev);
         next_time+=host->InitData.m_StoreInterval;
         host->ItemsInBuff += (host->ItemsInBuff^host->MaxItems)?1:0;}
         prev = rcvd;}

con_error:
    close(host->SelfSocket);
    host->SelfSocket = -1;
 }
 return NULL;
}

//==============================================================================================================================================================

int
NavigationPrivate::GetPosOnTime (struct NaviDataMsg& pos, struct timespec& _time)
{
   struct NaviDataMsg a;
   struct NaviDataMsg b;
          pos.Time = _time;

 pthread_mutex_lock(&SelfMutex);

  if ( pos.Time<OLD_TIME ) {
     printf("Navi: Return -1 OLD_TIME=%lu:%lu Qtime=%lu:%lu \n",OLD_TIME.tv_sec,OLD_TIME.tv_nsec,_time.tv_sec,_time.tv_nsec);
     pthread_mutex_unlock(&SelfMutex);
     return -1;}

  if (pos.Time>LST_TIME)
   {
      if (ItemsInBuff<2) {
          pthread_mutex_unlock(&SelfMutex);
          return -1;}

      a = CurItem->Prev->Data;
      b = CurItem->Data;
   }
  else
   {
      NDListItem* item = CurItem;
      while(pos.Time<item->Data.Time) item=item->Prev;
      a = item->Data;
      b = item->Next->Data;
   }

  pthread_mutex_unlock(&SelfMutex);

  double k = (pos.Time-a.Time)/(b.Time - a.Time);
  pos.Geo.B = a.Geo.B + (b.Geo.B-a.Geo.B)*k;
  pos.Geo.L = a.Geo.L + (b.Geo.L-a.Geo.L)*k;
  pos.Geo.H = a.Geo.H + (b.Geo.H-a.Geo.H)*k;
  pos.Q     = a.Q  + (b.Q  - a.Q)*k;
  pos.V     = a.V  + (b.V  - a.V)*k;
  pos.Ve    = a.Ve + (b.Ve - a.Ve)*k;
  pos.Vn    = a.Vn + (b.Vn - a.Vn)*k;
  ConvBLH2XYZ(pos.Geo,pos.Dec);

 // printf("B=%g L=%g H=%g X=%g Y=%g Z=%g K=%g\n",pos.Geo.B,pos.Geo.L,pos.Geo.H,pos.Dec.X,pos.Dec.Y,pos.Dec.Z,k);
  return 0;
}

//=========================================================================================================================

int
NavigationPrivate::GetCurPosition (struct NaviDataMsg& pos)
{
   struct NaviDataMsg a;
   struct NaviDataMsg b;
   TSystem->GetCurrentTime(pos.Time);

   pthread_mutex_lock(&SelfMutex);

   if ( pos.Time<OLD_TIME ) {
       printf("Navi: Return -1 OLD_TIME=%lu:%lu Qtime=%lu:%lu \n",OLD_TIME.tv_sec,OLD_TIME.tv_nsec,pos.Time.tv_sec,pos.Time.tv_nsec);
       pthread_mutex_unlock(&SelfMutex);
       return -1;}

   if (pos.Time>LST_TIME)
     {
       if (ItemsInBuff<2) {
          pthread_mutex_unlock(&SelfMutex);
          return -1;}

      a = CurItem->Prev->Data;
      b = CurItem->Data;
    // printf("Navi: LST_TIME=%lu:%lu Qtime=%lu:%lu \n",LST_TIME.tv_sec,LST_TIME.tv_nsec,pos.Time.tv_sec,pos.Time.tv_nsec);
    // struct timespec dtime = pos.Time-LST_TIME;
    // printf("Navi: Warning: Forecasting data. Time difference = %lu:%lu (%g sec)\n", dtime.tv_sec, dtime.tv_nsec,TSPEC2DBL(dtime));
     }
   else
     {
      NDListItem* item = CurItem;
      while(pos.Time<item->Data.Time) item=item->Prev;
      a = item->Data;
      b = item->Next->Data;
     }
   pthread_mutex_unlock(&SelfMutex);

   double k = (pos.Time-a.Time)/(b.Time - a.Time);

   pos.Geo.B = a.Geo.B + (b.Geo.B-a.Geo.B)*k;
   pos.Geo.L = a.Geo.L + (b.Geo.L-a.Geo.L)*k;
   pos.Geo.H = a.Geo.H + (b.Geo.H-a.Geo.H)*k;
   pos.Q     = a.Q  + (b.Q  - a.Q)*k;
   pos.V     = a.V  + (b.V  - a.V)*k;
   pos.Ve    = a.Ve + (b.Ve - a.Ve)*k;
   pos.Vn    = a.Vn + (b.Vn - a.Vn)*k;
   ConvBLH2XYZ(pos.Geo,pos.Dec);

   return 0;
}

//=============================================================================================================================================================

bool
NavigationPrivate::IsDataReady ( struct timespec& _time )
{
// printf("T:%lu:%lu  OLD:%lu:%lu LST:%lu:%lu Itms:%d \n",_time.tv_sec,_time.tv_nsec,OLD_TIME.tv_sec,OLD_TIME.tv_nsec,LST_TIME.tv_sec,LST_TIME.tv_nsec,ItemsInBuff);
 return (!(_time<OLD_TIME || (_time>LST_TIME && ItemsInBuff<2)));
}

//=============================================================================================================

Navigation::Navigation()
{

}

//=============================================================================================================

Navigation::~Navigation()
{

}

//=============================================================================================================

Navigation*
Navigation::NewNavigationClient(NaviInit& _init_data, bool autostart)
{
 return new NavigationPrivate(_init_data,autostart);
}

//=============================================================================================================

}
}
