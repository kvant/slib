#include "DataBuffer.h"
#include "string.h"
#include "stdlib.h" 
#include "stdio.h" 
namespace slib
{
//===========================================================================================

DataBuffer::DataBuffer()
{
        BuffSize = 0;
        ItemSize = 0;
            Wait = false;
      BuffPolicy = BP_CICLED;

 PtrStorageBegin = NULL;
      ItemsBegin = NULL;      
        ItemsEnd = NULL;
       ItemWrite = NULL;
        ItemRead = NULL; 
}

//===========================================================================================               

DataBuffer::~DataBuffer()
{
 FlushBuffer();
}

//===========================================================================================               

DataBuffer::DataBuffer (size_t n_itms, size_t itm_sz, BufferPolicy policy)
{
        BuffSize = n_itms;
        ItemSize = itm_sz;
      BuffPolicy = policy;

 PtrStorageBegin = NULL;
      ItemsBegin = NULL;      
        ItemsEnd = NULL;
       ItemWrite = NULL;
        ItemRead = NULL; 

  InitBuffer();
}

//===========================================================================================

int  
DataBuffer::InitBuffer  (void)
{
 
 if (!(BuffSize&&ItemSize)) return -1;

 FlushBuffer();

 ItemsBegin      = (BufferItem*) calloc(BuffSize,sizeof(BufferItem));  
 PtrStorageBegin = calloc (BuffSize,ItemSize);

  for (uint32_t i = 0; i<BuffSize;ItemsBegin[i].data = (char*)PtrStorageBegin + i*ItemSize,i++);

  ItemsEnd   = &ItemsBegin[BuffSize-1];
  ItemWrite  = ItemsBegin;
  ItemRead   = ItemsBegin;

  ////**** init sync params  
  pthread_mutexattr_init (&MutexAttr);
  pthread_mutex_init     (&SyncMutex,&MutexAttr);
  sem_init(&SyncSemaphore,0,BuffSize);  
  pthread_cond_init(&SelfCondVar,NULL);
  ////**** ~init_sync_params 

  SetPolicy(BuffPolicy);   
 
  return 0;
}

//===========================================================================================
          
int  
DataBuffer::FlushBuffer ()
{
 if (ItemsBegin)      free(ItemsBegin);
 if (PtrStorageBegin) free(PtrStorageBegin);

 PtrStorageBegin = NULL;
      ItemsBegin = NULL;      
        ItemsEnd = NULL;
       ItemWrite = NULL;
        ItemRead = NULL;      

 pthread_mutex_destroy(&SyncMutex);
 sem_destroy(&SyncSemaphore);  

 return 0;
}
          
//===========================================================================================

int  
DataBuffer::SetItemSize (size_t itm_sz)
{
 ItemSize = itm_sz;
 return 0;
}
          
//===========================================================================================

int  
DataBuffer::SetBufferSize (size_t n_itms)
{
 BuffSize = n_itms;
 return 0;
}
          
//===========================================================================================

int  
DataBuffer::SetPolicy (BufferPolicy policy)
{
  int err = -1;
  BuffPolicy = policy;
  pthread_mutex_lock(&SyncMutex); 
  switch (BuffPolicy)
    {
    case BP_CICLED:
        {
            CurPushMethod  = &DataBuffer::PushCicled;
            CurPushMethodV = &DataBuffer::PushCicledV;    
            CurPullMethod  = &DataBuffer::PullCicled;
            err^=err; 
            break;   
        }
    case BP_BLOCKED:
        {
            CurPushMethod  = &DataBuffer::PushBlocked;
            CurPushMethodV = &DataBuffer::PushBlockedV;
            CurPullMethod  = &DataBuffer::PullBlocked;
            err^=err;         
            break;
        }
     }
 pthread_mutex_unlock(&SyncMutex);
 return err;
}
       
//===========================================================================================

size_t  
DataBuffer::GetItemSize (void) 
const
{
 return ItemSize;
}
       
//===========================================================================================

size_t  
DataBuffer::GetBufferSize (void) 
const
{
 return BuffSize;
}      
       
//===========================================================================================

size_t  
DataBuffer::GetNumItems (void) 
{
 int s_val;
 sem_getvalue(&SyncSemaphore,&s_val);
 return (BuffSize-s_val);
}
 
//===========================================================================================

int   
DataBuffer::GetPolicy (void) 
const
{
 return BuffPolicy;
}
          
//===========================================================================================

int  
DataBuffer::IsEmpty(void)
{
 int s_val;
 sem_getvalue(&SyncSemaphore,&s_val);
 return (BuffSize^((size_t)s_val))?0:1;
}
          
//===========================================================================================

int  
DataBuffer::PushData (void* data, size_t size, uint32_t flags)
{
// printf("Now in buffer %d items\n",GetNumItems());
 return 
 (this->*CurPushMethod)(data,size,flags);
}
   
//===========================================================================================

int  
DataBuffer::PushDataV (iovec* vec, size_t _vec_size, uint32_t flags)
{
 return 
 (this->*CurPushMethodV)(vec,_vec_size,flags);
}
       
//===========================================================================================

int  
DataBuffer::PullData (void* data, size_t max_size, size_t& d_size, uint32_t flags)
{
 return 
 (this->*CurPullMethod)(data,max_size,d_size,flags);
}
          
//===========================================================================================

int  
DataBuffer::PullDataTimed (void* data, size_t max_size, size_t& d_size, struct timespec f_time, uint32_t flags)
{
 return 0;
}

//===========================================================================================

int  
DataBuffer::PushCicled (void* data, size_t size, uint32_t flags)
{
 size_t it_size = (size>ItemSize)?ItemSize:size;   
 pthread_mutex_lock(&SyncMutex);
 if (sem_trywait(&SyncSemaphore))
   {
        IncPtr(ItemRead);
        clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));      
        memcpy(ItemWrite->data,data,it_size);
        ItemWrite->size = it_size;
        IncPtr(ItemWrite);        
   } 
 else
   {
     clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));     
     memcpy(ItemWrite->data,data,it_size);
     ItemWrite->size = it_size;
     IncPtr(ItemWrite);   
   } 
  if (Wait) pthread_cond_signal(&SelfCondVar);
  pthread_mutex_unlock(&SyncMutex); 
  return 0;
}

//===========================================================================================

int  
DataBuffer::PushCicledV (struct iovec* _vec, size_t _vec_size, uint32_t flags)
{
 uint32_t _c    = _vec_size;
 size_t s_size  = 0;
 char*  ptr;
 if   (!_c)  return -1;
 while(_c--) s_size+=_vec[_c]._size;
 if (s_size>ItemSize) return -1;     
 
 pthread_mutex_lock(&SyncMutex);
 if (sem_trywait(&SyncSemaphore))
   {
    IncPtr(ItemRead);
    clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));      
    
     ptr = (char*) ItemWrite->data;
     _c  = 0;
     do
     {memcpy(ptr,_vec[_c]._data,_vec[_c]._size);
      ptr+=_vec[_c]._size;} while(++_c^_vec_size);

    ItemWrite->size = s_size;
    IncPtr(ItemWrite);        
   } 
 else
   {
     clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));     
     ptr = (char*)ItemWrite->data;
     _c = 0;
     do{ memcpy(ptr,_vec[_c]._data,_vec[_c]._size);
         ptr+=_vec[_c]._size;}while(++_c^_vec_size);
     ItemWrite->size = s_size;   
     IncPtr(ItemWrite);   
   } 
  if (Wait) pthread_cond_signal(&SelfCondVar);
  pthread_mutex_unlock(&SyncMutex); 
  return 0;
}
    
//===========================================================================================

int  
DataBuffer::PullCicled (void* data, size_t max_size, size_t& d_size, uint32_t flags)
{
 register int s_val;
 pthread_mutex_lock(&SyncMutex);
 sem_getvalue(&SyncSemaphore,&s_val);
 
 if (!(s_val^BuffSize)) 
   {
    if (flags&BF_NBLOCK){
     pthread_mutex_unlock(&SyncMutex);
     return -1;}
     Wait = true;
     pthread_cond_wait(&SelfCondVar,&SyncMutex);
   }
 d_size = ItemRead->size;     
 memcpy(data,ItemRead->data,d_size);
 IncPtr(ItemRead);
 sem_post(&SyncSemaphore); 
 pthread_mutex_unlock(&SyncMutex);  
 return 0;
}

//===========================================================================================

int  
DataBuffer::PushBlocked (void* data, size_t size, uint32_t flags)
{
 if (!(data&&size)) return -1;
 size_t it_size = (size>ItemSize)?ItemSize:size;   
 
if (flags & BF_NBLOCK)
   {
     if(sem_trywait(&SyncSemaphore)) return -1; 
   } 
 else
      sem_wait(&SyncSemaphore);

 clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));     
 memcpy(ItemWrite->data,data,it_size);
 ItemWrite->size = it_size;
 IncPtr(ItemWrite);
 return 0;
}
   
//===========================================================================================

int  
DataBuffer::PushBlockedV (struct iovec* _vec, size_t _vec_size, uint32_t flags)
{
 uint32_t _c    = _vec_size;
 size_t s_size  = 0;


 if   (!_c)  return -1;
 while(_c--) s_size+=_vec[_c]._size;
 if (s_size>ItemSize) return -1;         


 if (flags & BF_NBLOCK)
   {
     if(sem_trywait(&SyncSemaphore)) return -1; 
   } 
 else
      sem_wait(&SyncSemaphore);

 clock_gettime(CLOCK_REALTIME,&(ItemWrite->push_time));     
 while(_vec_size--){
   memcpy(ItemWrite->data,_vec[_vec_size]._data,_vec[_vec_size]._size);
   ItemWrite->data = (char*)ItemWrite->data + _vec[_vec_size]._size;}
 
 ItemWrite->size = s_size;  
 IncPtr(ItemWrite);
 return 0;
}
       
//===========================================================================================

int  
DataBuffer::PullBlocked (void* data, size_t max_size, size_t& d_size, uint32_t flags)
{
 register int s_val;
 sem_getvalue(&SyncSemaphore,&s_val);
 if (!(s_val^BuffSize)) return -1;
 d_size = ItemRead->size;     
 memcpy(data,ItemRead->data,d_size);
 IncPtr(ItemRead);
 sem_post(&SyncSemaphore);
 return 0;
}

//===========================================================================================

}
