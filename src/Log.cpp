#include <Log.h>

#include "pthread.h" 
#include "string.h"
#include "time.h"
#include "unistd.h"
#include "fcntl.h"
#include <errno.h>
#include <cstdlib>
#include <cstdio>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>


namespace slib {
namespace log  {

static int getFPath(string& res, const char* fname)
{
  char  _fname[256];
  char  command[512];
  char* _pos;
  char   _cwd[256];
  char  _cwdn[256];
  int   fd;
  string fullpath;

  if(getcwd(_cwd, 256));

 if( (_pos = strrchr(const_cast<char*>(fname),'/'))){
   strcpy(_fname,_pos+1);
   strncpy(_cwdn,fname,(_pos-fname));
   _cwdn[(_pos-fname)]='\0';
   if (access(_cwdn,R_OK)){
     switch (errno)
      {
       case EACCES:
          fprintf(stderr,"Permission denied to access directory: %s\n",_cwdn);
          return -1;
       case ENOTDIR:
       case ENOENT:
          fprintf (stderr,"No directory: %s\nTry to create --> ",_cwdn);
          sprintf (command,"mkdir -p %s",_cwdn);
          if(system(command)){};
          if(access(_cwdn,R_OK)){
            fprintf(stderr,"Directory no created\n");
            perror("Create dir");
            return -1;}
          else{
            fprintf(stderr,"Directory created\n");}
          break;  
      }
    }
   }  
 else{
   strcpy(_fname,fname);
   strcpy(_cwdn,"./");}

  
  fd = open(fname,O_WRONLY|O_CREAT,(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
  if (fd==-1)
   {
     fprintf(stderr,"Cannot open file for write: '%s'\n",fname);
     return -1;
   }
  close(fd);
  if(chdir(_cwdn));
  if(getcwd(_cwdn,256));
  fullpath = _cwdn;
  fullpath+="/";
  fullpath+=_fname;
  close(fd);
  if(chdir(_cwd));       
  res = fullpath;
  return 0;
}

//=================================================================================================

struct __locked_stream
{
                  int  id;
                  int  links;
                  int  std;
               string  fname;
            ofstream*  out;
      pthread_mutex_t  lock;
 inline void   Lock()  {pthread_mutex_lock(&lock);}
 inline void Unlock()  {pthread_mutex_unlock(&lock);}
    __locked_stream()  {pthread_mutex_init(&lock, NULL); links=0; std=0;}
   ~__locked_stream()  {pthread_mutex_destroy(&lock);}
};

//=================================================================================================

class SLogSess_p
{
                   uint32_t    Flags;
                   uint32_t    Level;                    
                       bool    Skip;
                     string    Name;
                   uint32_t    Id;
                friend class   SLog_p;  
                friend class   SLog::SLogSess;
                      friend   SLog::SLogSess& LOperator(SLog::SLogSess& sess, LogOperators oper, uint32_t level);
    vector<__locked_stream*>   Streams;

            public:
                               SLogSess_p    ( );
                               SLogSess_p    ( const SLogSess_p& src );           
                              ~SLogSess_p    ( );
        template <typename T>  SLogSess_p&  operator<<(T a);
                         void  NewStr        ( void );
                         void  Lock          ( void );                   
                         void  Unlock        ( void );     
                         void  Flush         ( void );  
                         void  InsertStream  ( __locked_stream* );
                         void  SetName       ( const char* name );
                         void  SetFlags      ( uint32_t flags);
                     uint32_t  GetFlags      ( void ) const; 
};

//=================================================================================================

SLogSess_p::SLogSess_p(const SLogSess_p& src)
{


}

//=================================================================================================

SLogSess_p::SLogSess_p():Flags(0), Level(0)
{
  Skip = 0;
}

//=================================================================================================

SLogSess_p::~SLogSess_p()
{
 uint32_t cnt = Streams.size();
 while(cnt--) {Streams[cnt]->Lock();Streams[cnt]->links--;Streams[cnt]->Unlock();}  
}

//=================================================================================================

template<typename T>
SLogSess_p&
SLogSess_p::operator<<(T a)
{
 uint32_t cnt = Skip?0:Streams.size();
 while(cnt--)  *(Streams[cnt]->out)<<a; 
 return *this;
}

//=================================================================================================

void  
SLogSess_p::NewStr(void)
{
// uint32_t cnt = Streams.size();
 struct timespec tsp;
 struct tm       tms;
 char  str[54];
 char* ptr = str;
 
 if (Flags & SLog::LF_TIMED)
   {
     clock_gettime(CLOCK_REALTIME,&tsp);
     gmtime_r(&tsp.tv_sec,&tms);
     sprintf(str,"[%04d:%04d] ", tms.tm_sec, (int)tsp.tv_nsec/100000); 
//     sprintf(str,"[%02d:%02d:%02d:%05d] ",tms.tm_hour,tms.tm_min,tms.tm_sec,(int)tsp.tv_nsec/10000); 
     ptr+=strlen(ptr);
   }

 if (Flags & SLog::LF_NAMED)
    sprintf(ptr,"[%s]",Name.c_str());  

 *this<<str;
}

//=================================================================================================

void  
SLogSess_p::Lock(void)
{
 uint32_t cnt = Streams.size();
 while(cnt--) Streams[cnt]->Lock();  
}

//=================================================================================================

void  
SLogSess_p::Unlock(void)
{
 uint32_t cnt = Streams.size();
 while(cnt--) Streams[cnt]->Unlock();  
}     

//=================================================================================================

void  
SLogSess_p::Flush(void)
{
 uint32_t cnt = Streams.size();
 while(cnt--) Streams[cnt]->out->flush();  
}     

//=================================================================================================

void  
SLogSess_p::InsertStream(__locked_stream* _strm)
{
 Streams.push_back(_strm);
 _strm->Lock(); 
 _strm->links++;
 _strm->Unlock();
}

//=================================================================================================

void  
SLogSess_p::SetName(const char* name)
{
 Name = name;
}

//================================================================================================

void  
SLogSess_p::SetFlags ( uint32_t flags)
{
 Lock();
  //fprintf(stderr,"Old flags = %d new = %d \n",Flags,flags);
  Flags = flags;
 Unlock(); 
}
                     
//================================================================================================
                     
uint32_t  
SLogSess_p::GetFlags ( void ) const
{
 return Flags;
}

//================================================================================================
//=================================================================================================
//=================================================================================================

/**
 */

class SLog_p
{
                   friend class  SLog;
                       uint32_t  Flags;
                       uint32_t  Level; 
       vector<__locked_stream*>  Streams;
   map<uint32_t,SLog::SLogSess>  Sessions;
                pthread_mutex_t  SelfMutex;
       
                
         __inline__  void Lock()   {pthread_mutex_lock(&SelfMutex);}     
         __inline__  void UnLock() {pthread_mutex_unlock(&SelfMutex);}

       inline int getIdByName(const char* name)
         {
            int cnt = Streams.size();
               while(cnt--) 
                  if (!Streams[cnt]->fname.compare(name)) break;
            return cnt;   
         }
                              

     inline void garbageCollection(void)
                {
                   vector<__locked_stream*>::iterator v_it = Streams.begin();
                   while(v_it!=Streams.end()) {
                      if ((*v_it)->std) {++v_it; continue;}
                      if (!(*v_it)->links){
                         (*v_it)->out->close();
                         delete (*v_it)->out;
                         delete *v_it;
                         Streams.erase(v_it);
                         v_it = Streams.begin();
                         continue;}
                   ++v_it; }
                 } 
              

         public:
                                 SLog_p();
                                ~SLog_p();

SLog::SLogSess& CreateSession(const char* name, int dlevel, const char* fname);
         void   AliasSession(SLog::SLogSess& sess, const char* fname);
         void   CloseSession(SLog::SLogSess& sess);
         void   SetLogFlags(uint32_t flags); 
};

//=============================================================================================

SLog_p::SLog_p()
{
   pthread_mutex_init(&SelfMutex,NULL);
   Streams.push_back(new __locked_stream());
   Streams.back()->fname = "stdout";
   Streams.back()->out   = static_cast<std::ofstream*>(&std::cout);    
   Streams.back()->std   = 1;
 
   Streams.push_back(new __locked_stream());
   Streams.back()->fname = "stderr";
   Streams.back()->out   = static_cast<std::ofstream*>(&std::cerr);    
   Streams.back()->std   = 1;   

   Flags = SLog::LF_TIMED | SLog::LF_NAMED | SLog::LF_ENABLE_ERR | SLog::LF_ENABLE_INFO | SLog::LF_ENABLE_WARN | SLog::LF_ENABLE_DBG;
   Level = 9;   
}

//=============================================================================================

SLog_p::~SLog_p()
{
 pthread_mutex_destroy(&SelfMutex); 
 Sessions.clear(); 
 garbageCollection();
}

//=============================================================================================

SLog::SLogSess&
SLog_p::CreateSession(const char* name, int dlevel, const char* fname)
{
  Lock();
           string  fpath;
         uint32_t  id = (Sessions.size() ? (Sessions.rbegin()->first + 1) : 0);
  SLog::SLogSess& res = Sessions[id];
  UnLock();

     res.d_ptr->Name  = name;
     res.d_ptr->Id    = id;
     res.d_ptr->SetFlags(Flags);
     res.d_ptr->Level = Level;

   AliasSession(Sessions[id], fname);

  return res; 
}
           
//=============================================================================================

void
SLog_p::AliasSession(SLog::SLogSess& sess, const char* fname)
{
 if ( !(fname && sess.d_ptr->Level) ) return;
   
 int cnt = getIdByName(fname);
 __locked_stream *lstr; 

 if ( cnt == -1 ) {
     lstr = new __locked_stream;
     getFPath(lstr->fname,fname);
     lstr->out = new ofstream();
     lstr->out->open(lstr->fname.c_str()); 
     Streams.push_back(lstr);  
     }
   else
        lstr = Streams[cnt];    

   sess.d_ptr->InsertStream(lstr);      
}
           
//=============================================================================================

void
SLog_p::CloseSession(SLog::SLogSess& sess)
{
  Sessions.erase(sess.d_ptr->Id); 
  garbageCollection();
};           

//=============================================================================================

void   
SLog_p::SetLogFlags(uint32_t flags)
{
  
// fprintf(stderr,"Set flags = %d\n",flags); 
 Lock();
  Flags = flags;
  map<uint32_t,SLog::SLogSess>::iterator m_it[]={Sessions.begin(), Sessions.end()};
  while(m_it[0]!=m_it[1])
  {
    m_it[0]->second.d_ptr->SetFlags(Flags);
  ++m_it[0];
  }
  
 UnLock();

};

//=================================================================================================
//=================================================================================================
//=================================================================================================


SLog::SLog():d_ptr(new SLog_p())
{

}

//=================================================================================================

SLog::~SLog()
{
 delete d_ptr;
}

//=================================================================================================

SLog::SLogSess& 
SLog::CreateSession(const char* name, int def_level, const char* f_name)
{
 return d_ptr->CreateSession(name, def_level,f_name);
}

//=================================================================================================

void 
SLog::SetLogLevel(int level)
{
d_ptr->Level = level;
}
          
//=================================================================================================

int 
SLog::GetLogLevel(void) 
const
{
return d_ptr->Level;
} 

//=================================================================================================

void 
SLog::SetLogFlags(uint32_t flags)
{
 d_ptr->SetLogFlags(flags);
}
         
//=================================================================================================

uint32_t 
SLog::GetLogFlags(void) 
const
{
 return d_ptr->Flags;
}     

//=================================================================================================

int 
SLog::AliasSession(SLogSess& sess, const char* f_name)
{
 d_ptr->AliasSession(sess,f_name);
 return 0;
}

//=================================================================================================

void
SLog::CloseSession(SLogSess& sess)
{
 d_ptr->CloseSession(sess);
}

//=================================================================================================

SLog::SLogSess::SLogSess():d_ptr(new SLogSess_p())
{
}

//=================================================================================================

SLog::SLogSess::SLogSess(const SLog::SLogSess& src):d_ptr(new SLogSess_p(*(src.d_ptr)))
{

}

//=================================================================================================

SLog::SLogSess::~SLogSess()
{
	delete d_ptr;
}

//=================================================================================================
/*
template<typename T>
SLog::SLogSess& 
SLog::SLogSess::operator<<(T a)
{
 *d_ptr<<a;
 return *this;
}*/

SLog::SLogSess& 
SLog::SLogSess::operator<<(int a)
{
 *d_ptr<<a;
  return *this;
}

SLog::SLogSess& 
SLog::SLogSess::operator<<(double a)
{
 *d_ptr<<a;
  return *this;
}

SLog::SLogSess& 
SLog::SLogSess::operator<<(const char* a)
{
 *d_ptr<<a;
  return *this;
}

SLog::SLogSess& 
SLog::SLogSess::operator<<(const string& a)
{
 *d_ptr<<a;
  return *this;
}

//=================================================================================================

SLog::SLogSess& 
SLog::SLogSess::operator<<(SLog::SLogSess&(*a)(SLog::SLogSess&))
{
 return a(*this);
}

void 
SLog::SLogSess::SetName (const char* name){
d_ptr->SetName(name);
}

//=================================================================================================

SLog::SLogSess&
LOperator(SLog::SLogSess& sess, LogOperators oper, uint32_t level)
{
  string  str;

switch (oper)
   {
      case LOG_OP_BEGIN_ERR:
           sess.d_ptr->Lock();
           if ((sess.d_ptr->Skip = (bool)!((sess.d_ptr->Flags)&SLog::LF_ENABLE_ERR))) goto skip;
           sess.d_ptr->NewStr();
           *(sess.d_ptr)<<"[E]: ";
           break;

      case LOG_OP_BEGIN_INFO:
           sess.d_ptr->Lock();
         //  fprintf(stderr,"Info print: flags = %d\n",sess.d_ptr->Flags);
           if ((sess.d_ptr->Skip = (bool)!((sess.d_ptr->Flags) & SLog::LF_ENABLE_INFO))) goto skip;
           sess.d_ptr->NewStr();
           *(sess.d_ptr)<<"[I]: ";
           break;

      case LOG_OP_BEGIN_WARN:
           sess.d_ptr->Lock();
           if ((sess.d_ptr->Skip = (bool)!((sess.d_ptr->Flags)&SLog::LF_ENABLE_WARN))) goto skip;
           sess.d_ptr->NewStr();
           *(sess.d_ptr)<<"[W]: ";
           break;

      case LOG_OP_BEGIN_DBG:
           sess.d_ptr->Lock();
           if ((sess.d_ptr->Skip = (bool)!((sess.d_ptr->Flags)&SLog::LF_ENABLE_DBG))) goto skip;
           sess.d_ptr->NewStr();
           *(sess.d_ptr)<<"[D]: ";
           break;

      case LOG_OP_FINISH:
           *(sess.d_ptr)<<"\n";
           sess.d_ptr->Unlock();
           sess.d_ptr->Flush();
          break;
   }

skip:

 return sess;
}


}}
