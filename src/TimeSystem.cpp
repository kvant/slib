#include <TimeSystem.h>
#include <TimeSupport.h>
#include "string.h"
#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>

#define MAX_INDEX 1024

#if EMBEDDED
#define PCI_DEVICE_PRESENT 1
#warning "Build with PCI time access support"
#endif 

using namespace std;

namespace slib     {
namespace services {

using namespace slib::timesupport;

class TimeSystemPriv: public TimeSystem
{

 struct timegroup
       {
          struct timespec  system_time;  
          struct timespec  extern_time;
        struct slib::timesupport::timepascal  extern_timep;   
       };
   
                int   CurTg;  
                int   Inited;  
                int   SocketPort;
                int   SelfSocket;
          pthread_t   SelfThread;
          pthread_t   EventThread;       

     pthread_cond_t   SelfCondVar;
    pthread_mutex_t   EventMutex;
    pthread_mutex_t   SelfMutex;
    struct timespec   MaxEvTime;
    struct timespec   MinEvTime;
    struct timespec   StartTime;   
   struct timegroup   LastCorrect[3];
struct TSInitData_t   InitData;

vector<struct sockaddr_in> RepeatEP; 
 
 vector<pair<uint32_t, timeEvent>>  EventsMap;
      /*unordered_*/set<uint32_t>  EvIndexes;

               int   StartUDP       (void);
               int   StartPCI       (void);
               int   StartEvents    (void);
       static void*  TimeListener   (void* arg);
       static void*  PCIListener   (void* arg);
       static void*  EventProcessor (void* arg);

 public:
                    TimeSystemPriv (struct TSInitData_t& _init_data, bool autostart = true);
                   ~TimeSystemPriv ();

  virtual      int  Start          (struct TSInitData_t* _init_data = NULL);
  virtual      int  Stop           (void);
  virtual uint32_t  InsertEvent    (struct timeEvent& event, uint flags = 0 );
  virtual     void  FlushEvents    (void);
  virtual     void  RemoveEvent    (uint32_t id, bool locked = true);
  virtual     void  GetCurrentTime (struct timespec& _time);
  virtual     void  GetCurrentTime (struct slib::timesupport::timepascal& _time);
  virtual
   struct timespec  GetCurrentTime (void);
  virtual     void  Wait4Ready     (void);

  virtual     void  InsertRepeatEP  (struct sockaddr_in& addr );
  virtual     void  RemoveRepeatEP  (struct sockaddr_in& addr );
  virtual     void  DropAllRepeatEP (void);

};


//===========================TIMESYSTEM()============================================================

TimeSystemPriv::TimeSystemPriv(struct TSInitData_t& _init_data, bool autostart): CurTg(0),Inited(0),SocketPort(0),SelfSocket(-1),SelfThread(0),EventThread(0)
                                  
{
	pthread_cond_init(&SelfCondVar, NULL);
	pthread_mutex_init(&EventMutex, NULL);
	pthread_mutex_init(&SelfMutex, NULL);
	clock_gettime(CLOCK_REALTIME, &LastCorrect[CurTg].system_time);
	LastCorrect[CurTg].extern_time = LastCorrect[CurTg].system_time;
	tspec2tpascal(LastCorrect[CurTg].extern_time, LastCorrect[CurTg].extern_timep);
	InitData = _init_data;

	for (uint32_t i = 1; i < MAX_INDEX; EvIndexes.insert(i++));

	if (autostart)
		Start(NULL);
}

//===================================================================================================

int TimeSystemPriv::Start(struct TSInitData_t* _init_data) 
{
// printf("Start time\n");
  Stop();
  
  if (_init_data) InitData = *_init_data; 


  switch(InitData.m_Mode)
   {
     case TSM_NET_LISTENER:
        if(StartUDP()) return -1;;
        break;
     case TSM_PCI_ACCESS:    
        if(StartPCI()) return -1;
     default:
        return -1;
    }   

  if (InitData.m_EventsEnable) 
    return StartEvents();

  return 0;
}

//===================================================================================================

int  
TimeSystemPriv::Stop (void)
{

 pthread_mutex_lock(&EventMutex);
  if (EventThread){
        pthread_cancel(EventThread);
        EventThread = 0;}
 pthread_mutex_unlock(&EventMutex);    
    
 pthread_mutex_lock(&SelfMutex);
  if(SelfThread){
      pthread_cancel(SelfThread);
      SelfThread = 0;}
 pthread_mutex_unlock(&SelfMutex);  

 if (SelfSocket!=-1){
   close(SelfSocket);
   SelfSocket = -1;}
   
#if (PCI_DEVICE_PRESENT)
 if (InitData.m_Mode ==  TSM_PCI_ACCESS ) uts_close();
#endif

 Inited = 0;
 FlushEvents();
 return 0;    
}

void*  
TimeSystemPriv::PCIListener (void* arg)
{
#ifdef PCI_DEVICE_PRESENT
 TimeSystemPriv* host = static_cast<TimeSystemPriv*>(arg);  
 register int index = host->CurTg ^ 1;
  
 struct uts_time_val tval; 
 struct timepascal   tpas; 

 pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
 pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
 uts_get_time(&tval); 
 tpas.tp_usec = tval.msec;
 tpas.tp_day  = tval.day;
   
 clock_gettime(CLOCK_REALTIME,&(host->LastCorrect[index].system_time));
 tpascal2tspec(host->LastCorrect[index].extern_timep,host->LastCorrect[index].extern_time);

 pthread_mutex_lock(&host->SelfMutex);
    host->CurTg = index;
 pthread_mutex_unlock(&host->SelfMutex); 
 
 host->Inited = 1;
 
 while(1)
  {
   index^=1;

   sleep(1);

   uts_get_time(&tval);

   tpas.tp_usec = tval.msec;
   tpas.tp_day  = tval.day;
   
   host->LastCorrect[index].extern_timep = tpas;
   
   clock_gettime(CLOCK_REALTIME,&(host->LastCorrect[index].system_time));
   tpascal2tspec(host->LastCorrect[index].extern_timep,host->LastCorrect[index].extern_time);

   pthread_mutex_lock(&host->SelfMutex);
    host->CurTg = index;
   pthread_mutex_unlock(&host->SelfMutex); 
  }   
#endif
  return NULL;
}


//===========================TimeListener(void* arg)============================================================

void*  
TimeSystemPriv::TimeListener(void* arg)
{
 TimeSystemPriv* host = static_cast<TimeSystemPriv*>(arg);
 register int index = host->CurTg ^ 1;
 int soc_copy = host->SelfSocket;
 uint32_t a;

 pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
 pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);

 if (0) {
 	recv(soc_copy,&(host->LastCorrect[index].extern_timep),sizeof(slib::timesupport::timepascal),0);
 	clock_gettime(CLOCK_REALTIME,&(host->LastCorrect[index].system_time));
 	tpascal2tspec(host->LastCorrect[index].extern_timep,host->LastCorrect[index].extern_time);
 } else {
 	clock_gettime(CLOCK_REALTIME, &(host->LastCorrect[index].system_time));
	host->LastCorrect[index].extern_time = host->LastCorrect[index].system_time;
//	tpascal2tspec(host->LastCorrect[index].extern_timep,host->LastCorrect[index].extern_time);
 }

 pthread_mutex_lock(&host->SelfMutex);
 host->CurTg = index;
 pthread_mutex_unlock(&host->SelfMutex);

 host->Inited = 1;

 while(1)
  {
   index^=1;
   recv(soc_copy,&(host->LastCorrect[index].extern_timep),sizeof(slib::timesupport::timepascal),0);
   clock_gettime(CLOCK_REALTIME,&(host->LastCorrect[index].system_time));
   tpascal2tspec(host->LastCorrect[index].extern_timep,host->LastCorrect[index].extern_time);
   pthread_mutex_lock(&host->SelfMutex);

    host->CurTg = index;
    a = host->RepeatEP.size();
    while(a--){
     sendto(  soc_copy,
              &(host->LastCorrect[index].extern_timep),
              sizeof(slib::timesupport::timepascal),0,
              (struct sockaddr*)&(host->RepeatEP[a]),
              sizeof(struct sockaddr_in) );
    }
   pthread_mutex_unlock(&host->SelfMutex);
  } 
 return NULL;
}

//===========================StartListen()============================================================

int  
TimeSystemPriv::StartPCI()
{
 pthread_attr_t attr;
 pthread_attr_init(&attr);
#if (PCI_DEVICE_PRESENT) 
 while (uts_open()) sleep(1);
#endif
 CurTg  = 0;
 Inited = 0;
 
 pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
 pthread_create(&SelfThread,&attr,PCIListener,this); 
 pthread_attr_destroy(&attr);
 return 0;
}

//===========================StartListen()============================================================

int  
TimeSystemPriv::StartUDP()
{
 pthread_attr_t attr;
 struct sockaddr_in addr;

 if((SelfSocket = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP))==-1) return -1;
 memset(&addr,0x00,sizeof(addr));
 addr.sin_port        = htons(InitData.m_Opts.Net.ListenPort);
 addr.sin_family      = AF_INET; 
 addr.sin_addr.s_addr = INADDR_ANY; 
 
 if (bind(SelfSocket,(struct sockaddr*)&addr,sizeof(addr))){
   close(SelfSocket);
   SelfSocket = -1;
   return -1;}

 CurTg  = 0;
 Inited = 0;
 pthread_attr_init(&attr);
 pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
 pthread_create(&SelfThread,&attr,TimeListener,this); 
 pthread_attr_destroy(&attr);

 return 0;
}

//===========================StartListen()============================================================

int   
TimeSystemPriv::StartEvents (void)
{
 pthread_attr_t attr;
 pthread_attr_init(&attr);
 pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
 pthread_create(&EventThread,&attr,EventProcessor,this); 
 pthread_attr_destroy(&attr);
 return 0;
}

//===========================~TimeSystem()============================================================

TimeSystemPriv::~TimeSystemPriv()
{
 Stop();
}

//===========================GetCurrentTime(struct timespec _time)============================================================
     
void 
TimeSystemPriv::GetCurrentTime(struct timespec& _time)
{
 struct timespec now;
 clock_gettime(CLOCK_REALTIME,&now);
 pthread_mutex_lock(&SelfMutex);
  _time = now-LastCorrect[CurTg].system_time+LastCorrect[CurTg].extern_time;
 pthread_mutex_unlock(&SelfMutex);
}
 
//============================================================================================================================ 
 
struct timespec
TimeSystemPriv::GetCurrentTime( void )
{
 struct timespec now;
 clock_gettime(CLOCK_REALTIME,&now);
 pthread_mutex_lock(&SelfMutex);
  now = now - LastCorrect[CurTg].system_time + LastCorrect[CurTg].extern_time;
 pthread_mutex_unlock(&SelfMutex);
 return now;
} 
 
//===========================GetCurrentTime(struct timespec _time)============================================================

void 
TimeSystemPriv::GetCurrentTime(struct slib::timesupport::timepascal& _time)
{
 struct timespec now;
 clock_gettime(CLOCK_REALTIME,&now);
 pthread_mutex_lock(&SelfMutex);
  now = now-LastCorrect[CurTg].system_time+LastCorrect[CurTg].extern_time;
 pthread_mutex_unlock(&SelfMutex); 
 tspec2tpascal(now,_time);  
}

void TimeSystemPriv::Wait4Ready(void)
{
	while(!Inited)
		sleep(1);
}

void* TimeSystemPriv::EventProcessor(void* arg)
{
	struct timespec wait;
	struct timespec sys_now;
	TimeSystemPriv* host = (TimeSystemPriv*)arg;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	pthread_mutex_lock(&host->EventMutex);

	while(1) {
		if (host->EventsMap.size()) {
			host->MinEvTime = host->EventsMap.front().second.te_time;
			host->GetCurrentTime(sys_now);
			clock_gettime(CLOCK_REALTIME, &wait);
			if (host->MinEvTime > sys_now)
				wait += (host->MinEvTime - sys_now);
		} else {
			clock_gettime(CLOCK_REALTIME, &wait);
			wait.tv_sec += 3600;
			host->MinEvTime.tv_sec = 0xFFFFFFFF;
		}

		switch(pthread_cond_timedwait(&host->SelfCondVar,
					&host->EventMutex, &wait)) {
		case 0:
			continue;
			break;

		case ETIMEDOUT:
			if (host->EventsMap.empty())
				continue;
			host->GetCurrentTime(sys_now);
			if (sys_now < host->MinEvTime)
				continue;
			host->EventsMap.begin()->second.te_cb(&host->EventsMap.begin()->second);
			host->EvIndexes.insert(host->EventsMap.begin()->first);
			host->EventsMap.erase(host->EventsMap.begin());
			break;

		default:
			break;
		}
	}

	return NULL;
}

uint32_t TimeSystemPriv::InsertEvent (struct timeEvent& event, uint flags)
{
	uint32_t ret;
	uint32_t cnt;

	pthread_mutex_lock(&EventMutex);

	if(!EventThread || EvIndexes.empty()) {
		pthread_mutex_unlock(&EventMutex);
		return 0;
	}

	ret = *(EvIndexes.begin());
	EvIndexes.erase(EvIndexes.begin());

	if (flags & TE_FL_TIME_RELATIVE)
		event.te_time += GetCurrentTime();

	if (EventsMap.size()) {
		if (event.te_time < MinEvTime) {
			EventsMap.insert(EventsMap.begin(),
					pair<uint32_t, timeEvent>(ret, event));
			MinEvTime = event.te_time;
			pthread_cond_signal(&SelfCondVar);
		} else {
			if (event.te_time > MaxEvTime) {
				EventsMap.push_back(pair<uint32_t, timeEvent>(ret, event));
				MaxEvTime = event.te_time;
			} else {
				cnt = EventsMap.size();
				while(EventsMap[--cnt].second.te_time > event.te_time);
				++cnt;
				EventsMap.insert((EventsMap.begin() + cnt),
						pair<uint32_t, timeEvent>(ret, event));
			}
		}
	} else {
		EventsMap.push_back(pair<uint32_t,timeEvent>(ret,event));
		MaxEvTime = MinEvTime = event.te_time;
		pthread_cond_signal(&SelfCondVar);
	}

	pthread_mutex_unlock(&EventMutex);
	return ret;
}

void TimeSystemPriv::RemoveEvent ( uint32_t id, bool locked )
{
	uint32_t i;
	uint32_t cnt;

	if (locked)
		pthread_mutex_lock(&EventMutex);

	if (EvIndexes.count(id) || id >= MAX_INDEX || (!id)) {
		if (locked)
			pthread_mutex_unlock(&EventMutex);
		fprintf(stderr, "Fake remove for %d \n", id);
		return;
	}

	cnt = EventsMap.size();

	for (i=0; (i < cnt) && (EventsMap[i].first != id); i++);

	if (i == cnt) {
		if (locked)
			pthread_mutex_unlock(&EventMutex);
		return;
	}

	EventsMap.erase((EventsMap.begin() + i));
	EvIndexes.insert(id);

	if (EventsMap.empty()) {
		MinEvTime.tv_sec = ~0x0;
		MaxEvTime.tv_sec = MaxEvTime.tv_nsec = 0;
	} else {
		MinEvTime = EventsMap.front().second.te_time;
		MaxEvTime = EventsMap.back().second.te_time;
	}

	if (!i)
		pthread_cond_signal(&SelfCondVar);

	if (locked)
		pthread_mutex_unlock(&EventMutex);
}

void TimeSystemPriv::FlushEvents ()
{

}

void TimeSystemPriv::InsertRepeatEP  ( struct sockaddr_in& addr )
{
 pthread_mutex_lock(&SelfMutex);
  //uint32_t cnt = RepeatEP.size();
  //for(uint32_t i=0;i<cnt;i++) 
   // if (!memcpy(&addr,&(RepeatEP[i]),sizeof(addr))){
   //  pthread_mutex_unlock(&SelfMutex);         
   //  return;}    
 RepeatEP.push_back(addr);
 pthread_mutex_unlock(&SelfMutex);  
}

//=======================================================================================

void  
TimeSystemPriv::RemoveRepeatEP  ( struct sockaddr_in& addr )
{
 pthread_mutex_lock(&SelfMutex);
  vector<struct sockaddr_in>::iterator v[2]={RepeatEP.begin(),RepeatEP.end()};
 while(v[0]!=v[1])
 {
    if(!memcpy(&(*v[0]),&addr,sizeof(addr))) {
    RepeatEP.erase(v[0]);
    pthread_mutex_unlock(&SelfMutex);
    return;}
   ++v[0]; 
 }   
 pthread_mutex_unlock(&SelfMutex);
}

//=======================================================================================

void
TimeSystemPriv::DropAllRepeatEP ( void )
{
 pthread_mutex_lock(&SelfMutex);
  RepeatEP.clear();
 pthread_mutex_unlock(&SelfMutex);
    
}


//=======================================================================================

TimeSystem*
TimeSystem::NewTimeSystem(struct TSInitData_t& _init_data, bool autostart)
{
  return new TimeSystemPriv(_init_data,autostart);  
}

//=======================================================================================

TimeSystem::TimeSystem()
{
}

//=======================================================================================

TimeSystem::~TimeSystem()
{
}

//=======================================================================================

}
}


