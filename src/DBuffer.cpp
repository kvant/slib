#include <DBuffer.h>
#include <string.h>
#include <stdlib.h>

DBuffer::DBuffer(size_t _items, size_t _itm_size):	BSize(_items),
							ISize(_itm_size),
							Mutex(PTHREAD_MUTEX_INITIALIZER)
{
	size_t cnt = _items-2;
	Data       = (Item*)calloc(_items,sizeof(Item));
	Data->data = calloc(_items,_itm_size);

	do {
		Data[cnt].Next = &Data[cnt+1];
		Data[cnt].data = (char*)Data->data+cnt*_itm_size;
	} while(cnt--);

	Data[_items-1].Next = Data;
	Data[_items-1].data = (char*)Data->data+(_items-1)*_itm_size;

	Wr = Rd = Data;

	sem_init(&SemRev,0,_items);
	sem_init(&SemFwd,0,0);
}

DBuffer::~DBuffer()
{
	free(Data->data);
	free(Data);
	sem_destroy(&SemRev);
	sem_destroy(&SemFwd);
}

int DBuffer::Push(void* data, size_t size, bool no_block)
{
	if (size > ISize)
		return -1;

	if (no_block) {
		if (sem_trywait(&SemRev))
			return -1;
	}
	else
		sem_wait(&SemRev);
	memcpy(Wr->data, data, size);
	Wr->size = size;
	Wr = Wr->Next;
	sem_post(&SemFwd);

	return 0;
}

int DBuffer::Pop(void *data, size_t max_size, size_t &r_size, bool no_block)
{
	if (no_block) {
		if (sem_trywait(&SemFwd))
			return -1;
	}
	else
		sem_wait(&SemFwd);

	memcpy(data, Rd->data, Rd->size);
	r_size = Rd->size;
	Rd = Rd->Next;
	sem_post(&SemRev);
	return 0;
}

void DBuffer::Reset(void)
{
	Wr = Rd = Data;
	sem_destroy(&SemRev);
	sem_destroy(&SemFwd);
	sem_init(&SemRev,0,BSize);
	sem_init(&SemFwd,0,0);
}

