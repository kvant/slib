#include <Geo.h>
#include <math.h>
#include <stdio.h>

namespace slib {
namespace geo  {

static struct EllipsoidParams locElParams;

//================================================================================//================================================================================

void 
__attribute__ ((constructor))
InitGeo(void)
{
 InitEllipsoide(0);
}

//================================================================================//================================================================================

void 
InitEllipsoide(int el_type)
{
 double t = 1.0/298.3;
 locElParams.a   = 6378.245;
 locElParams.a2  = locElParams.a*locElParams.a;
 locElParams.b   = locElParams.a - locElParams.a*t;
 locElParams.b2  = locElParams.b*locElParams.b;
 locElParams.e12 = (locElParams.a2 - locElParams.b2) / locElParams.a2;
 locElParams.B_Precise = 1e-4*M_PI/(3600 * 180);
}

//================================================================================//================================================================================

static
double
__attribute__((always_inline))
CalcN(double B)
{
//a_N := EllipsoidParameters.a / Sqrt (1 - EllipsoidParameters.e12 * Sqr (sin (B)));
 register double a = sin(B);
 return locElParams.a/sqrt(1-locElParams.e12*a*a);
}

//================================================================================//================================================================================

void
ConvBLH2XYZ(struct CoordGeo& geo, struct CoordDec3D& dec)
{
 double a_N = CalcN(geo.B);
 double a_D = (a_N+geo.H)*cos(geo.B);
      dec.X = a_D*cos(geo.L);
      dec.Y = a_D*sin(geo.L);
      dec.Z = ((1-locElParams.e12)*a_N+geo.H)*sin(geo.B);
}

//================================================================================//================================================================================

void
ConvXYZ2BLH(struct CoordDec3D& dec, struct CoordGeo& geo)
{
  double a_D = hypot(dec.X,dec.Y);
  double a_R;
  double a_B; 
  double a_C;
  double a_P;
  double sinB;
  double cosB;
  double a_S1;
  double a_dS;
  double a_S2; 
 
  if (a_D==0.0)
   {
      geo.B = M_PI * dec.Z / (2.0 * fabs (dec.Z));
       sinB = sin(geo.B);
      geo.L = 0.0;
      geo.H = dec.Z*sinB - locElParams.a*sqrt(1 - locElParams.e12*sinB*sinB);
   }
  else
   {
      geo.L = asin(dec.Y/a_D);
      geo.L = ((dec.Y<0.0) && (dec.X>0.0))?(M2PI-geo.L):(((dec.Y<0.0) && (dec.X<0.0))?(M2PI+geo.L):(((dec.Y>0.0) && (dec.X<0.0))?(M_PI-geo.L):(geo.L)));
      
      if (dec.Z==0.0)
         {
           geo.B = 0.0;
           geo.H = a_D - locElParams.a;
         }
      else
         {
           a_R = sqrt(dec.X*dec.X + dec.Y*dec.Y + dec.Z*dec.Z);
           a_C = asin(dec.Z/a_R);
           a_P = locElParams.e12*locElParams.a/(a_R+a_R);
           
          a_S1 = 0.0;
            do
             {
               a_B = a_C + a_S1;
              sinB = sin(a_B);
              a_S2 = asin(a_P*sin(a_B+a_B)/sqrt(1 - locElParams.e12*sinB*sinB));
              a_dS = fabs(a_S2-a_S1);
              a_S1 = a_S2;  
             }
            while(a_dS>locElParams.B_Precise);  
          
          geo.B = a_B;
          sincos(geo.B,&sinB,&cosB);    
          geo.H = a_D*cosB + dec.Z*sinB - locElParams.a*sqrt(1-locElParams.e12*sinB*sinB);    
         }
   }  
}

//================================================================================//================================================================================

void 
ApplyMatrix  ( struct CoordDec3D& xyz, CMatrix_t& matrix)
{
  xyz.X*=1e-3;
  xyz.Y*=1e-3;
  xyz.Z*=1e-3;
 struct CoordDec3D  result  = {
                               matrix [1][0] * xyz.Y + \
                               matrix [1][1] * xyz.X + \
                               matrix [1][2] * xyz.Z + \
                               matrix [1][3],             \
                                                          \
                               matrix [0][0] * xyz.Y + \
                               matrix [0][1] * xyz.X + \
                               matrix [0][2] * xyz.Z + \
                               matrix [0][3],

                               matrix [2][0] * xyz.Y + \
                               matrix [2][1] * xyz.X + \
                               matrix [2][2] * xyz.Z + \
                               matrix [2][3]
                              }; 
 xyz = result;
 xyz.X*=1e3;
 xyz.Y*=1e3;
 xyz.Z*=1e3;
}

void 
ApplyMatrixXYZ  ( struct CoordDec3D& xyz, CMatrix_t& matrix)
{
  xyz.X*=1e-3;
  xyz.Y*=1e-3;
  xyz.Z*=1e-3;
 struct CoordDec3D  result  = {
                               matrix [0][0] * xyz.X + \
                               matrix [0][1] * xyz.Y + \
                               matrix [0][2] * xyz.Z + \
                               matrix [0][3],          \
                                                       \
                               matrix [1][0] * xyz.X + \
                               matrix [1][1] * xyz.Y + \
                               matrix [1][2] * xyz.Z + \
                               matrix [1][3],

                               matrix [2][0] * xyz.X + \
                               matrix [2][1] * xyz.Y + \
                               matrix [2][2] * xyz.Z + \
                               matrix [2][3]
                              }; 
 xyz = result;
 xyz.X*=1e3;
 xyz.Y*=1e3;
 xyz.Z*=1e3;
}



void 
ConvXYZ2XYZ(struct CoordDec3D& center,struct CoordDec3D& dest, CMatrix_t& matrix)
{

 dest.X = matrix [0][0] * center.X + \
          matrix [0][1] * center.Y + \
          matrix [0][2] * center.Z + \
          matrix [0][3];
         
 dest.Y = matrix [1][0] * center.X + \
          matrix [1][1] * center.Y + \
          matrix [1][2] * center.Z + \
          matrix [1][3];
          
 dest.Z = matrix [2][0] * center.X + \
          matrix [2][1] * center.Y + \
          matrix [2][2] * center.Z + \
          matrix [2][3]; 
 
/* dest.Y = matrix [0][0] * center.Y + \
          matrix [0][1] * center.X + \
          matrix [0][2] * center.Z + \
          matrix [0][3];
         
 dest.X = matrix [1][0] * center.Y + \
          matrix [1][1] * center.X + \
          matrix [1][2] * center.Z + \
          matrix [1][3];
          
 dest.Z = matrix [2][0] * center.Y + \
          matrix [2][1] * center.X + \
          matrix [2][2] * center.Z + \
          matrix [2][3];*/
}

//================================================================================//================================================================================

void
TranslateV_kms ( struct Velocity& V_src,struct Velocity& V_dst, CMatrix_t& matrix)
{
 V_dst.Vx = matrix[0][0]*V_src.Vx + matrix[0][1]*V_src.Vy + matrix[0][2]*V_src.Vz;
 V_dst.Vy = matrix[1][0]*V_src.Vx + matrix[1][1]*V_src.Vy + matrix[1][2]*V_src.Vz;
 V_dst.Vz = matrix[2][0]*V_src.Vx + matrix[2][1]*V_src.Vy + matrix[2][2]*V_src.Vz;
 V_dst.Vabs = V_src.Vabs;
}

//================================================================================//================================================================================

void
TranslateRMS_km ( struct RMS& RMS_src,struct RMS& RMS_dst, CMatrix_t& matrix)
{
 double sqr[3]={RMS_src.RMSx*RMS_src.RMSx,RMS_src.RMSy*RMS_src.RMSy,RMS_src.RMSx*RMS_src.RMSx};
 
 RMS_dst.RMSx = sqrt(matrix[0][0]*matrix[0][0]*sqr[0] + matrix[0][1]*matrix[0][1]*sqr[1] + matrix[0][2]*matrix[0][2]*sqr[2]);
 RMS_dst.RMSy = sqrt(matrix[1][0]*matrix[1][0]*sqr[0] + matrix[1][1]*matrix[1][1]*sqr[1] + matrix[1][2]*matrix[1][2]*sqr[2]);
 RMS_dst.RMSz = sqrt(matrix[2][0]*matrix[2][0]*sqr[0] + matrix[2][1]*matrix[2][1]*sqr[1] + matrix[2][2]*matrix[2][2]*sqr[2]);
 RMS_dst.RMSxyz = RMS_src.RMSxyz;
}


//================================================================================//================================================================================

void xyz2BLH (struct CoordGeo& src_pos, struct CoordDec3D& obj_pos_xyz, struct CoordGeo& obj_pos_blh)
{
  struct CoordDec3D tmp_dec = {X:obj_pos_xyz.X * 1e-3, Y:obj_pos_xyz.Y * 1e-3, Z:obj_pos_xyz.Z * 1e-3};
  struct CoordDec3D res;
  CMatrix_t matrix;

  CalcCMatrix(src_pos,matrix);
  ConvXYZ2XYZ(tmp_dec,res,matrix);
  ConvXYZ2BLH(res,obj_pos_blh);
  obj_pos_blh.H*=1e3;
}

//================================================================================//================================================================================

void BLH2xyz ( struct CoordGeo& center, struct CoordGeo& obj_pos_blh, struct CoordDec3D& obj_pos_xyz ) {
  CMatrix_t matrix;
  obj_pos_xyz.X = obj_pos_xyz.Y = obj_pos_xyz.Z = 0.0;
  CalcCMatrix(obj_pos_blh,center,matrix);
  ApplyMatrix(obj_pos_xyz,matrix);
}

//================================================================================//================================================================================

void CalcCMatrix(struct CoordGeo& src, struct CoordGeo& dest, CMatrix_t& matrix)
{
 double sinL1,cosL1;
 double sinB1,cosB1;
 double sinL2,cosL2;
 double sinB2,cosB2; 
 double sindL,cosdL;

 struct CoordDec3D dsrc;
 struct CoordDec3D ddest;

 ConvBLH2XYZ(src, dsrc);
 ConvBLH2XYZ(dest,ddest);

 dsrc.X-=ddest.X;
 dsrc.Y-=ddest.Y; 
 dsrc.Z-=ddest.Z; 

 sincos(src.L,&sinL2,&cosL2);  
 sincos(src.B,&sinB2,&cosB2);
 sincos(dest.L,&sinL1,&cosL1);
 sincos(dest.B,&sinB1,&cosB1);
 sincos((src.L - dest.L),&sindL,&cosdL);
 
 matrix [0][0] =  sinB1*sinB2*cosdL + cosB1*cosB2;
 matrix [0][1] =  sinB1*sindL;
 matrix [0][2] = -sinB1*cosB2*cosdL + cosB1*sinB2; 

 matrix [1][0] =  -sinB2*sindL;
 matrix [1][1] =  cosdL;
 matrix [1][2] =  cosB2*sindL;

 matrix [2][0] = -cosB1*sinB2*cosdL + sinB1*cosB2;
 matrix [2][1] = -cosB1*sindL;
 matrix [2][2] =  cosB1*cosB2*cosdL + sinB1*sinB2;

 matrix [0][3] = -sinB1*(dsrc.X*cosL1 + dsrc.Y*sinL1) + cosB1*dsrc.Z;
 matrix [1][3] = -dsrc.X*sinL1 + dsrc.Y*cosL1;
 matrix [2][3] =  cosB1*(dsrc.X*cosL1 + dsrc.Y*sinL1) + sinB1*dsrc.Z;
}

//================================================================================//================================================================================

void 
CalcCMatrix(struct CoordGeo& src, CMatrix_t& matrix)
{
  double sinL,cosL;
  double sinB,cosB;
  struct CoordDec3D xyz;

  sincos(src.L,&sinL,&cosL);
  sincos(src.B,&sinB,&cosB); 
  
  ConvBLH2XYZ(src,xyz);

  matrix [0][0] = -sinB*cosL;
  matrix [0][1] = -sinL;
  matrix [0][2] =  cosB*cosL;

  matrix [1][0] = -sinB*sinL;
  matrix [1][1] = cosL;
  matrix [1][2] = cosB*sinL;

  matrix [2][0] = cosB;
  matrix [2][1] = 0;
  matrix [2][2] = sinB;

  matrix [0][3] = xyz.X;
  matrix [1][3] = xyz.Y;
  matrix [2][3] = xyz.Z;
}

//================================================================================//================================================================================

void 
CalcCMatrix(struct CoordGeo& src, struct CoordDec3D& dec, CMatrix_t& matrix)
{
  double sinL,cosL;
  double sinB,cosB;

  sincos(src.L,&sinL,&cosL);
  sincos(src.B,&sinB,&cosB); 
  
  matrix [0][0] = -sinB*cosL;
  matrix [0][1] = -sinL;
  matrix [0][2] =  cosB*cosL;

  matrix [1][0] = -sinB*sinL;
  matrix [1][1] = cosL;
  matrix [1][2] = cosB*sinL;

  matrix [2][0] = cosB;
  matrix [2][1] = 0;
  matrix [2][2] = sinB;

  matrix [0][3] = dec.X;
  matrix [1][3] = dec.Y;
  matrix [2][3] = dec.Z;
}

//================================================================================//================================================================================

void
ConvXYH2XYZ(struct CoordDec3D& xyh, struct CoordGeo& src)
{
 double a_B;
 double a_R_B0;
 double a_R_B;
 double a_R_H;
 double sinB,cosB;
 double sinaB,cosaB;

 sincos(src.B,&sinB,&cosB);

 a_R_B0 = sqrt (locElParams.a2*cosB*cosB + locElParams.b2*sinB*sinB);
    a_B = src.B + xyh.X/a_R_B0;
  sincos(a_B,&sinaB,&cosaB);

  a_R_B = sqrt (locElParams.a2*cosaB*cosaB + locElParams.b2*sinaB*sinaB);
  a_R_H = a_R_B + xyh.Z;
  xyh.Z = a_R_H * cos(hypot(xyh.X,xyh.Y)/a_R_H) - a_R_B0 - src.H;
}

//=============================================================

////
void 
CrossCircle (double Px, double Py, double q,double r, std::vector<CoordDec2D>& pnt_vec)
{ 
  pnt_vec.clear();
  struct CoordDec2D point;
 
  if (q == 0.0 || q == M_PI){
    if (Px>r) return;  
    if (Px==r) { 
      point.X  = r; 
      point.Y  = 0;
      pnt_vec.push_back(point); 
      return;}
  
     point.X = Px;
     point.Y = sqrt(r*r - Px*Px);
     pnt_vec.push_back(point); 

     point.Y = -point.Y;
     pnt_vec.push_back(point); 
     return;}
  
  double Px1 = sin(q)+Px; 
  double Py1 = cos(q)+Py;   
  
  double k = (Py1 - Py)/(Px1-Px);
  double b =  Py1 - k*Px1;
  
  double d=(pow((2*k*b/*-2*Px-2*Py*k*/),2)-(4+4*k*k)*(b*b-r*r/*+Px*Px+Py*Py-2*Py*b*/));
  if(d<0) return;
  double x1=((-(2*k*b/*-2*Px-2*Py*k*/)-sqrt(d))/(2+2*k*k));
  double x2=((-(2*k*b/*-2*Px-2*Py*k*/)+sqrt(d))/(2+2*k*k));
  
     
  if (x1==x2)  {point.X = x1; point.Y = k*x1+b; pnt_vec.push_back(point); return;} 
 
  point.X  = x1;
  point.Y  = k*x1+b;
  pnt_vec.push_back(point);
 
  point.X  = x2;
  point.Y  = k*x2+b;
  pnt_vec.push_back(point);
 
  return;
}

int CrossLines (CoordDec2D p11, ANGLE_TYPE q1, CoordDec2D p21, ANGLE_TYPE q2, CoordDec2D& cr_p)
{

  CoordDec2D p12 = {p11.X+sin(q1),p11.Y+cos(q1)};   
  CoordDec2D p22 = {p21.X+sin(q2),p21.Y+cos(q2)};   

  double Z  = (p12.Y-p11.Y)*(p21.X-p22.X)-(p21.Y-p22.Y)*(p12.X-p11.X);
  double Ca = (p12.Y-p11.Y)*(p21.X-p11.X)-(p21.Y-p11.Y)*(p12.X-p11.X);
  double Cb = (p21.Y-p11.Y)*(p21.X-p22.X)-(p21.Y-p22.Y)*(p21.X-p11.X);

  if( (Z == 0)&&(Ca == 0)&&(Cb == 0) )
    {
//         result.type = ctSameLine;
//         return result;
        return -1;
    }

    if( Z == 0 )
    {
       // result.type = ctParallel;
       // return result;
       return -1;
    }

    double Ub = Cb/Z;

    cr_p.X = p11.X + (p12.X - p11.X) * Ub;
    cr_p.Y = p11.Y + (p12.Y - p11.Y) * Ub;

  return 0;
}


int
CrossLinesSharp (CoordDec2D p11, CoordDec2D p12, CoordDec2D p21, CoordDec2D p22, CoordDec2D& cr_p)
{

  double Z  = (p12.Y-p11.Y)*(p21.X-p22.X)-(p21.Y-p22.Y)*(p12.X-p11.X);
  double Ca = (p12.Y-p11.Y)*(p21.X-p11.X)-(p21.Y-p11.Y)*(p12.X-p11.X);
  double Cb = (p21.Y-p11.Y)*(p21.X-p22.X)-(p21.Y-p22.Y)*(p21.X-p11.X);

  if( (Z == 0)&&(Ca == 0)&&(Cb == 0) )
    {
//         result.type = ctSameLine;
//         return result;
        return -1;
    }

    if( Z == 0 )
    {
       // result.type = ctParallel;
       // return result;
       return -1;
    }


    double Ua = Ca/Z;
    double Ub = Cb/Z;

    cr_p.X = p11.X + (p12.X - p11.X) * Ub;
    cr_p.Y = p11.Y + (p12.Y - p11.Y) * Ub;

     if( (0 <= Ua)&&(Ua <= 1)&&(0 <= Ub)&&(Ub <= 1) )
     {
        return 0; 
     }
     else
     {
        return -1;
     }
  
  return 0; 
} 
   
}
}
