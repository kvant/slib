#include <Config.h>
#include <cstring>
#include "arpa/inet.h"


namespace slib{
namespace config {

  
/////////////////////////////////////////////////////////////////
//// [section_name]
//// {
////   val1="dfff"
////   val2="dfff1"
////   #comment 
//// }
  
/////////////////////////////////////////////////////////////////  

class ConfigSectionPriv:public ConfigSection
 {
   public:
     typedef vector<pair<string,vector<ConfigSection*> > >::iterator sec_it_t;
     typedef vector<pair<string,vector<string> > >::iterator         val_it_t; 
   
   protected:
     
   friend class ConfigSection;
   
                             ConfigSectionPriv (const char* name);
                                       string  Name;
                                     uint32_t  Level;
        vector<pair<string,vector<string> > >  Values;
vector<pair<string,vector<ConfigSection*> > >  SubSections;
   
   sec_it_t  
   searchSection(const char* name)
   __attribute((always_inline))
    {
      uint32_t cnt = SubSections.size(); 
      while(cnt--)   
        if (!SubSections[cnt].first.compare(name)) return SubSections.begin()+cnt;
      return SubSections.end();
    }

   val_it_t  
   searchValue(const char* name)
   __attribute((always_inline))
   {
    uint32_t cnt = Values.size(); 
    while(cnt--)   
      if (!Values[cnt].first.compare(name)) return Values.begin()+cnt;
    return Values.end();
   }
   
 protected:       
   
                      int   isComment         (string& str);
                      int   isSpaced          (string& str);
                      int   isSectionStart    (string& str); 
                      int   isSectionName     (string& str);
                      int   isSectionFinish   (string& str);  
                      int   GetSectionName    (string& src, string& name);
                      int   ParseString       (string& src);
                      int   InsertSubSection  (ifstream& _str,const char* name);

           ConfigSection*   GetSectionByPath  (const char* path);       
                    
   public: 
                           ~ConfigSectionPriv (); 
                            
 virtual              int    ParseFromStream  (ifstream& _str);
 virtual              int    SaveToStream     (ofstream& data);
 
 virtual         uint32_t    GetNumValues     (const char* name);
 virtual         uint32_t    GetNumSections   (const char* name);
 virtual             void    GetValuesNames   (vector<string>& vstr);
 virtual             void    GetSectionsNames (vector<string>& vstr);

 virtual   ConfigSection*    GetSection       (const char* name, uint32_t id);
 virtual   ConfigSection*    GetSectionEx     (const char* name);
 virtual             void    RmSection        (const char* name, uint32_t id);
 virtual             void    RmSectionArr     (const char* name);
 virtual    ConfigSection*   AddSection       (const char* name);
 virtual              int    AddValue         (const char* name);
 virtual              int    RmValue          (const char* name, uint32_t id);
 virtual              int    RmValueArr       (const char* name);
 virtual              int    SetInt           (const char* name, uint32_t id, int val);
 virtual              int    SetDouble        (const char* name, uint32_t id, double val);
 virtual              int    SetString        (const char* name, uint32_t id, const char* val);
 virtual              int    GetInt           (const char* name, uint32_t id, int& val);
 virtual              int    GetIntV          (const char* name, uint32_t id, std::vector<int>& );     
 virtual              int    GetDouble        (const char* name, uint32_t id, double& val);
 virtual              int    GetString        (const char* name, uint32_t id, char* val, int maxlen);     
 virtual              int    SetIntEx         (const char* name, int val);
 virtual              int    SetDoubleEx      (const char* name, double val);
 virtual              int    SetStringEx      (const char* name, const char* val);
 virtual              int    GetIntEx         (const char* name, int& val);
 virtual              int    GetDoubleEx      (const char* name, double& val);
 virtual              int    GetStringEx      (const char* name, char* val, int maxlen);     
 virtual              int    GetEndPointEx    (const char* name,  struct sockaddr_in&);     
 virtual              int    GetEndPoint      (const char* name, uint32_t id, struct sockaddr_in&);     
 virtual              int    GetIntVEx         (const char* name,  std::vector<int>& );     
 virtual             void    Flush            (void);
 };
 
//======================================================================================================================================
 
 ConfigSection* 
 ConfigSectionPriv::GetSectionByPath (const char* path)
 {
   uint32_t id = 0;
   char self_name[128]={0}; 
   sec_it_t sec_it;
   if (!strlen(path)) return NULL;
   char* d_pos = strchr(const_cast<char*>(path),'.'); 
   if (d_pos)
   {
    strncpy(self_name,path,d_pos-path);
   }
   else
   {
    strcpy(self_name,path);
   }

   char* b_pos=strchr(self_name,'[');
    if (b_pos)
     {
      char* e_pos = strchr(b_pos,']');
      if (!e_pos) return NULL;
      *b_pos++ = '\0';
      char sid[16];
      strncpy(sid,b_pos,e_pos-b_pos);
      id = atoi(sid);
     }
  
   sec_it = searchSection(self_name);
   if (sec_it == SubSections.end())  return NULL;
   if (sec_it->second.size()<(id+1)) return NULL;
   if (d_pos)  return static_cast<ConfigSectionPriv*>(sec_it->second[id])->GetSectionByPath(d_pos+1);
   return sec_it->second[id];
 }
 
//======================================================================================================================================

 void    
  ConfigSectionPriv::Flush (void)
 {
   for (uint32_t cnt=0;cnt<SubSections.size();cnt++)
    for (uint32_t cnt1=0;cnt1<SubSections[cnt].second.size();cnt1++)
     delete SubSections[cnt].second[cnt1];
   SubSections.clear();
   Values.clear();
 }

//======================================================================================================================================
 
 int    
 ConfigSectionPriv::SaveToStream (ofstream& data)
 {
  data<<endl<<"["<<Name<<"]"<<endl<<"{"<<endl;
  for(uint32_t cnt=0;cnt<Values.size();cnt++)
    for(uint32_t cnt1=0;cnt1<Values[cnt].second.size();cnt1++)
       data<<Values[cnt].first<<"=\""<<Values[cnt].second[cnt1]<<"\""<<endl;
      
  for(uint32_t cnt=0;cnt<SubSections.size();cnt++)    
    for(uint32_t cnt1=0;cnt1<SubSections[cnt].second.size();cnt1++)      
       SubSections[cnt].second[cnt1]->SaveToStream(data);
   
  data<<"}"<<endl; 
  return 0;  
 }
 
 
int 
ConfigSectionPriv::ParseString     (string& src)
{
  uint32_t pos_a;
  uint32_t pos_b; 
  val_it_t val_it;
  string   name;
  string   val;
  for (pos_a=0;isspace(src[pos_a]);pos_a++);
  if (!isalpha(src[pos_a])) return -1;
  if ((pos_b = src.find('=',pos_a))==string::npos)  return -1;
  while(!isalpha(src[pos_b])) --pos_b;
  name = src.substr(pos_a,pos_b-pos_a+1);
  if ((pos_a = src.find("\"",pos_b))==string::npos) return -1;
  ++pos_a;
  if ((pos_b = src.find("\"",pos_a))==string::npos) return -1;
  val = src.substr(pos_a,pos_b-pos_a);
  if((val_it = searchValue(name.c_str()))==Values.end())
    {
     pair<string,vector<string> > new_p;
     new_p.first = name;
     new_p.second.push_back(val);
     Values.push_back(new_p); 
    }
  else
    {
      val_it->second.push_back(val);
    } 
  return 0;
}
 
int 
ConfigSectionPriv::isSpaced (string& str)
{
 uint32_t cnt = str.size(); 
 while(cnt--)
   if (!isspace(str[cnt])) return 0;
 return 1;  
}
 
int 
ConfigSectionPriv::isComment      (string& str)
{
  uint32_t pos_a;
  for (pos_a=0;isspace(str[pos_a]);pos_a++);
  return  (str.find('#')==pos_a || str.find("//")==pos_a)?1:0;
}

int 
ConfigSectionPriv::isSectionName (string& str)
{
  uint32_t pos_a,pos_b;
  
  if ((pos_a = str.find('['))==string::npos) return 0;
  if ((pos_b = str.find(']'))==string::npos) return 0;
  if  (pos_b<pos_a)     return 0;  
  do {if (!isalpha(str[++pos_a])) return 0;}while(pos_a^pos_b);
  return 1;
}

int 
ConfigSectionPriv::isSectionStart  (string& str)
{
  return (str.find('{')==string::npos)?0:1;
}

int 
ConfigSectionPriv::isSectionFinish (string& str)
{
  return (str.find('}')==string::npos)?0:1;
}

int 
ConfigSectionPriv::GetSectionName (string& str, string& name)
{
  uint32_t pos_a,pos_b;
  if ((pos_a = str.find('['))==string::npos) return -1;
  if ((pos_b = str.find(']'))==string::npos) return -1;
  if  (pos_b<pos_a)     return -1;  
  ++pos_a;
  name = str.substr(pos_a,pos_b-pos_a);
  if (!name.size()) return -1;
  --pos_b;
 // while(pos_a^pos_b) {if (!isalpha(str[pos_a++])) return -1;};
  return 0;
}
 
int 
ConfigSectionPriv::InsertSubSection(ifstream& _str, const char*  name)
{
  ConfigSection* new_sect = ConfigSection::CreateSection(name);
  new_sect->ParseFromStream(_str);
  sec_it_t sec_it = searchSection(name);
  if (sec_it== SubSections.end())
   {
    pair<string,vector<ConfigSection*> > new_pair;
    new_pair.first = name;
    new_pair.second.push_back(new_sect);
    SubSections.push_back(new_pair);
   }
  else
   {
    sec_it->second.push_back(new_sect);    
   }
  return 0; 
}
 
int 
ConfigSectionPriv::ParseFromStream (ifstream& _str)
{
               bool   good;
             string   line; 
             string   line1;
             string   line2;
 pair<string,string>  param_pair;
 
 while(!_str.eof())
  {
   getline(_str,line);    
   ///check for comment
   if (isSpaced(line) || isComment(line) || line.empty()) continue;
   if (isSectionFinish(line)) break;
 
   if (ParseString(line))
    {
     if (GetSectionName(line,line1))
       {
        cerr<<"Warning: error line "<<line<<" - skiping"<<endl;
        continue;
       }
      good = false;
      while(!_str.eof()) 
       {
         getline(_str,line2);
         if (isSectionStart(line2)) {good = true;break;}
         if (isSectionFinish(line2) || !GetSectionName(line2,line)) break;
       }
      if (!good)
       {
         cerr<<"Error: begin of section"<< line1 <<" not found"<<endl;
       }
       InsertSubSection(_str,line1.c_str());
    }
  }
return 0;
}
 
ConfigSectionPriv::ConfigSectionPriv(const char* name):ConfigSection(),Name(name)
{

}
 
ConfigSectionPriv::~ConfigSectionPriv()
{
  Flush();
}

uint32_t   
ConfigSectionPriv::GetNumValues (const char* name)
{
  val_it_t val_it = searchValue(name);
  return ((val_it==Values.end())?0:val_it->second.size());
}

uint32_t   
ConfigSectionPriv::GetNumSections (const char* name) 
{
  sec_it_t sec_it = searchSection(name);
  return ((sec_it==SubSections.end())?0:sec_it->second.size());
}

void   
ConfigSectionPriv::GetValuesNames (vector<string>& vstr)
{
  uint32_t cnt = Values.size();
  vstr.clear();
  while(cnt) vstr.push_back(Values[--cnt].first);
}
              
void   
ConfigSectionPriv::GetSectionsNames (vector<string>& vstr)
{
  uint32_t cnt = SubSections.size();
  vstr.clear();
  while(cnt) vstr.push_back(SubSections[--cnt].first);
}

ConfigSection*   
ConfigSectionPriv::GetSection (const char* name, uint32_t id)
{
  sec_it_t sec_it = searchSection(name);
  if (sec_it == SubSections.end()) return NULL;
  if (sec_it->second.size()<(id+1)) return NULL;
  return sec_it->second[id];
}

ConfigSection*    
ConfigSectionPriv::GetSectionEx  (const char* name)
{
 return GetSectionByPath(name);
}

void    
ConfigSectionPriv::RmSection (const char* name, uint32_t id)
{
  sec_it_t sec_it = searchSection(name);
  if (sec_it == SubSections.end())  return;
  if (sec_it->second.size()<(id+1)) return;
  delete sec_it->second[id];
  sec_it->second.erase((sec_it->second.begin()+id));
}

void    
ConfigSectionPriv::RmSectionArr (const char* name)
{
  uint32_t cnt;
  sec_it_t sec_it = searchSection(name);
  if (sec_it == SubSections.end())  return;
  cnt = sec_it->second.size();
  while(cnt--)
    delete sec_it->second[cnt];
  SubSections.erase(sec_it);
}

ConfigSection*   
ConfigSectionPriv::AddSection (const char* name)
{
  sec_it_t sec_it = searchSection(name);
  ConfigSection* new_sect = ConfigSection::CreateSection(name);
  
  if (sec_it==SubSections.end())
   {
     pair<string,vector<ConfigSection*> > new_p;
     new_p.first = name;
     new_p.second.push_back(new_sect);
     SubSections.push_back(new_p);
   }
   else
   {
     sec_it->second.push_back(new_sect);
   }  
  return new_sect;
}

int    
ConfigSectionPriv::AddValue (const char* name)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end())
 {
  pair<string,vector<string> > new_p;
  new_p.first = name;
  new_p.second.push_back(string());
  Values.push_back(new_p);
 }
 else
 {
  val_it->second.push_back(string());
 }
 return 0;
}

int    
ConfigSectionPriv::RmValue (const char* name, uint32_t id)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return 0;
 if (val_it->second.size()<(id+1)) return 0;
 val_it->second.erase(val_it->second.begin()+id);
 return 0;
}

int    
ConfigSectionPriv::RmValueArr (const char* name)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return 0;
 Values.erase(val_it);
 return 0;
}

int    
ConfigSectionPriv::SetInt (const char* name, uint32_t id, int val)
{
 char buff[128];
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 sprintf(buff,"%d",val);
 val_it->second[id]=string(buff);
 return 0;
}

int    
ConfigSectionPriv::SetDouble (const char* name, uint32_t id, double val)
{
 char buff[128];
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 sprintf(buff,"%g",val);
 val_it->second[id]=string(buff);
 return 0;  
}

int    
ConfigSectionPriv::SetString (const char* name, uint32_t id, const char* val)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 val_it->second[id]=string(val); 
 return 0;  
}

int    
ConfigSectionPriv::GetInt (const char* name, uint32_t id, int& val)
{
  val_it_t val_it = searchValue(name);
  if (val_it==Values.end()) return -1;
  if (val_it->second.size()<(id+1)) return -1;
  val = atoi(val_it->second[id].c_str());
  return 0;
}

int    
ConfigSectionPriv::GetDouble (const char* name, uint32_t id, double& val)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 val = atof(val_it->second[id].c_str()); 
 return 0;
}

int    
ConfigSectionPriv::GetString (const char* name, uint32_t id, char* val, int maxlen)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 if (val_it->second[id].size()>=static_cast<size_t>(maxlen)) return -1;
 strcpy(val,val_it->second[id].c_str());
 return 0;
}

int    
ConfigSectionPriv::SetIntEx (const char* path, int val)
{
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   return sess->SetInt(name,id,val);
}

int    
ConfigSectionPriv::SetDoubleEx (const char* path, double val)
{
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   return sess->SetDouble(name,id,val);
}

int    
ConfigSectionPriv::SetStringEx (const char* path, const char* val)
{
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
  
   return sess->SetString(name,id,val);
}

int    
ConfigSectionPriv::GetIntEx (const char* path, int& val)
{
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   return sess->GetInt(name,id,val);
}

int    
ConfigSectionPriv::GetDoubleEx (const char* path, double& val)
{
 uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   return sess->GetDouble(name,id,val);
}

int    
ConfigSectionPriv::GetEndPointEx (const char* path,  struct sockaddr_in& val)
{
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   
   return sess->GetEndPoint(name,id,val);
}

//=============================================================================================

int 
ConfigSectionPriv::GetIntVEx (const char* path,  std::vector<int>& val ) { 
  
  uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   
   return sess->GetIntV(name,id,val); }

//=============================================================================================

int    
ConfigSectionPriv::GetIntV (const char* name, uint32_t id, std::vector<int>& val){
 
 bool p = false; 
 val.clear();
 
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 
 char tdata[val_it->second[id].size()+1];
 char* ptr = tdata + sizeof(tdata);
 strcpy(tdata,val_it->second[id].c_str());

//  

 while( --ptr != tdata ) {
   if (isdigit(*ptr)){ p = true; continue; }
   if ( p ) { 
   int itm = atoi(ptr+1);
   val.push_back(itm); }
   *ptr = '\0'; 
   p = false; }
 
 if (p){
   int itm = atoi(ptr);
   val.push_back(itm); }
 
 return 0;}

//=============================================================================================

int    
ConfigSectionPriv::GetEndPoint (const char* name, uint32_t id, struct sockaddr_in& arg)
{
 val_it_t val_it = searchValue(name);
 if (val_it==Values.end()) return -1;
 if (val_it->second.size()<(id+1)) return -1;
 if (string::npos == val_it->second[id].find(':')) return -1;
 
 memset(&arg,0x00,sizeof(arg));

 arg.sin_family      = AF_INET;
 arg.sin_addr.s_addr = inet_addr(val_it->second[id].substr(0,val_it->second[id].find(':')).c_str());
 arg.sin_port        = htons((unsigned short)atoi(val_it->second[id].substr(val_it->second[id].find(':')+1).c_str()));
 
 return (arg.sin_addr.s_addr==0xFFFFFFFF)?-1:0; 
}


int    
ConfigSectionPriv::GetStringEx (const char* path, char* val, int maxlen)
{
 uint32_t id = 0;
  char* d_pos = strrchr(const_cast<char*>(path),'.');
  char  name[1024];
  ConfigSection* sess = this;
  
  if (d_pos)
   {
     strncpy(name,path,d_pos-path);     
     name[d_pos-path]='\0';
     if(!(sess = GetSectionByPath(name))) return -1;
     strcpy(name,d_pos+1);
   }
   else 
   {
     strcpy(name,path);
   }

   if ((d_pos = strchr(name,'[')))
   {
     char* e_pos = strchr(name,']');
     if (!e_pos) return -1;
     *e_pos='\0';
     id = atoi(d_pos+1);
     *d_pos='\0';
   }
   return sess->GetString(name,id,val,maxlen); 
} 
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ConfigSection::ConfigSection()
{

}
                 
ConfigSection::~ConfigSection()
{

}

ConfigSection*     
ConfigSection::CreateSection (const char* name)
{
 return new ConfigSectionPriv(name);
}

void      
ConfigSection::DestroySection   (ConfigSection* section)
{
 delete section;
}
  
/////////////////////////////////////////////////////////////////  


class ConfigPriv:public Config
  {
     friend class Config;
     ConfigPriv();  
    ~ConfigPriv();
    
        ConfigSection* RootSection;
    
    public:

      virtual             int  LoadConfig     (const char* fname);
      virtual             int  SaveConfig     (const char* fname);
      virtual        uint32_t  GetNumSections (const char* name);
      virtual  ConfigSection*  GetRoot        (void);
  };

ConfigPriv::ConfigPriv():Config(),RootSection(ConfigSection::CreateSection("root"))
{
 
}  
  
ConfigPriv::~ConfigPriv()
{
 if (RootSection) delete RootSection;
}

int  
ConfigPriv::LoadConfig (const char* fname)
{
 ifstream _str;
 if (RootSection)
  {
   delete RootSection;
   RootSection = NULL;
  }
  
  _str.open(fname);
  if (!_str.is_open())
   {
    cerr<<"Can't open file: "<<fname<<endl;
    return -1;
   }
   
  RootSection = ConfigSection::CreateSection("root"); 
  RootSection->ParseFromStream(_str);
  _str.close();

  return 0;
}

int  
ConfigPriv::SaveConfig (const char* fname)
{
 if (!RootSection) return -1;
 ofstream _str;
 _str.open(fname);
 if (!_str.is_open())
  {
    cerr<<"Error: can't open stream for write. Name: "<<fname<<endl;
    return -1;
  }
  RootSection->SaveToStream(_str);
  _str.flush();
  _str.close();
  return 0;
}

uint32_t  
ConfigPriv::GetNumSections (const char* name)
{

return 0;
}

ConfigSection*  
ConfigPriv::GetRoot (void)
{
 return RootSection;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

Config*  
Config::NewConfig (void)
{
 return new ConfigPriv;
}

Config::Config ()
{

}
 
Config::~Config ()
{

}
  
void  
Config::DestroyConfig  (Config* _conf)
{
 if (_conf) delete _conf;
}

}}

